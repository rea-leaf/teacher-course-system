package com.tcs.log.reducer;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
* @Title: EtlLogReducer.java
* @Package com.tcs.log.reducer
* @author 神经刀
* @date 2018年4月7日
* @version V1.0
 */
public class EtlLogReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

	@Override
	protected void reduce(Text key, Iterable<IntWritable> intWritable,
			Context context) throws IOException, InterruptedException {
		context.write(key,new IntWritable(0));
	}
}