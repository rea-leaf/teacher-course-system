/**
 * index
 */

$(document).bind('mobileinit',function(){
    $.mobile.changePage.defaults.changeHash = false;
    $.mobile.hashListeningEnabled = false;
    $.mobile.pushStateEnabled = false;
});

var regionId = "";

function getRealContentHeight() {  
    var header = $.mobile.activePage.find("div[data-role='header']:visible");  
    var footer = $.mobile.activePage.find("div[data-role='footer']:visible");  
    var content = $.mobile.activePage.find("div[data-role='content']:visible:visible");  
    var viewport_height = $(window).height();  
    var content_height = viewport_height - header.outerHeight() - footer.outerHeight();  
    if((content.outerHeight() - header.outerHeight() - footer.outerHeight()) <= viewport_height) {  
        content_height -= (content.outerHeight() - content.height());  
    }
    return content_height;  
}  

// / 设置高度的函数
var fixgeometry = function() {
	var content = $.mobile.activePage.find("div[data-role='content']:visible:visible");
	content.height(getRealContentHeight());
};

$(document).on("pageinit", "#tuopo", function() {
	$("#A").on("click",clickFun);
	$("#B").on("click",clickFun);
	$("#C").on("click",clickFun);
	$("#D").on("click",clickFun);
	$("#E").on("click",clickFun);
	$(window).bind("orientationchange resize pageshow", fixgeometry);
});

$(document).on("pageshow","#selectGoods",function() {
	postAjax("http://wswangbo007.tpddns.cn:8002/teacher-corese-system-web/goodsController/queryRegionGoodList",
			{regionId: regionId},
			function (data) {
				if (data) {
					var append = "";
					var classArray = ["ui-block-a","ui-block-b","ui-block-c"];
					var index = 0;
					for (var i = 0,count = data.length; i < count; i++,index++) {
						append += getRow(classArray[index], "cai", data[i].goodsId, data[i].goodsName,data[i].status);
						if (index == 2) {
							index = 0;
						}
					}
					console.debug("append : {}" , append);
					$("#goodsShowDiv").empty();
					$("#goodsShowDiv").append(append);
					$("div[name='cai']").on('click',clickCai);
					rendering($("div[name='cai']"));		// 重新渲染
				}
			},
			function(data) {
				requestError();
			}
	);
	$(window).bind("orientationchange resize pageshow", fixgeometry);
});

/**
 * 渲染
 */
var rendering = function(runderId) {
	$.each(runderId,function(index, obj) {
		var status = $(obj).attr("type");
		if (status == 1) {
			$(obj).addClass("add_red");
		}
	});
}

function postAjax(url,data,successFun,errorFun) {
	$.ajax({
		type : "POST",
		url: url,
		data : data,
		dataType : "json",
		success : successFun,
		error : errorFun
	});
}

function getRow(classVal,name,inputName,value,type) {
	var row = "<div class='"+ classVal +"' name='"+ name +"' type='"+ type +"'><span>"+ value +"</span><input type='hidden' value='"+ inputName +"'></div>";
	return row;
}

/**
 *  记录员
 */
var clickCai = function(type) {
	var status = $(this).attr('type');
	var goodsId = $(this).find("input").val();
	var object = $(this);
	if (status == 0) {
		postAjax("http://wswangbo007.tpddns.cn:8002/teacher-corese-system-web/goodsController/addGoodsById",
				{"status" : status, "goodsId" : goodsId},
				function(data) {
					if (data) {
						if (data.result == 1) {
							object.addClass("add_red");
							object.attr('type',"1");
						} else if (data.result == 0) {
							alert("删除失败!");
						}
					}
				} , 
				function(data) {
					requestError();
				}
		);
	} else if (status == 1) {
		postAjax("http://wswangbo007.tpddns.cn:8002/teacher-corese-system-web/goodsController/removeGoodsbyId",
				{"status" : status, "goodsId" : goodsId},
				function(data) {
					if (data) {
						if (data.result == 1) {
							object.removeClass("add_red");
							object.attr('type',"0");
						} else if (data.result == 0) {
							alert("删除失败!");
						}
					}
				} , 
				function(data) {
					requestError();
				}
		);
	}
};

/**
 * 采购货物
 */
$(document).on("pageshow","#caigouView",function() {
	$("#search").on('click' , caigoucaiSearch);
	$(window).bind("orientationchange resize pageshow", fixgeometry);
});

var caigoucaiSearch = function() {
	var date = $("#dateInput").val();
	postAjax("http://wswangbo007.tpddns.cn:8002/teacher-corese-system-web/goodsController/queryCaiGouGoodsListByDate",
			{"date": date},
			function (data) {
				if (data) {
					var name = "cai_caigou";
					var append = "";
					var classArray = ["ui-block-a","ui-block-b","ui-block-c"];
					var index = 0;
					for (var i = 0,count = data.length; i < count; i++,index++) {
						append += getRow(classArray[index], name, data[i].id, data[i].goodsName,data[i].status);
						if (index == 2) {
							index = 0;
						}
					}
					console.debug("append : {}" , append);
					$("#goodsShowDiv_caigou").empty();
					$("#goodsShowDiv_caigou").append(append);
					$("div[name='cai_caigou']").on('click', seccessGoods);
					rendering($("div[name='cai_caigou']"));		// 重新渲染
				}
			},
			function(data) {
				requestError();
			});
};


var seccessGoods = function() {
	var status = $(this).attr('type');
	var id = $(this).find("input").val();
	var object = $(this);
	if (status == 0) {
		postAjax("http://wswangbo007.tpddns.cn:8002/teacher-corese-system-web/goodsController/updateGoodsStatusById",
				{"id" : id, "status" : 1},
				function(data) {
					if (data) {
						if (data.status == 1) {
							object.addClass("add_red");
							object.attr('type',"1");
						} else if (data.status == 0) {
							alert("删除失败!");
						}
					}
				} , 
				function(data) {
					requestError();
				}
		);
	} else if (status == 1) {
		postAjax("http://wswangbo007.tpddns.cn:8002/teacher-corese-system-web/goodsController/updateGoodsStatusById",
				{"id" : id, "status" : 0},
				function(data) {
					if (data) {
						if (data.status == 1) {
							object.removeClass("add_red");
							object.attr('type',"0");
						} else if (data.status == 0) {
							alert("删除失败!");
						}
					}
				} , 
				function(data) {
					requestError();
				}
		);
	}
}


var requestError = function() {
	alert("请求失败");
}

var clickFun = function() {
	alert("您选择了" + $(this).find("input").val() + "区");
	regionId = $(this).find("input").val();
};



/**
 * 采购货物
 */
$(document).on("pageshow","#GuanliView",function() {
	$("#sunmitGoods").on('click' , addGoods);
	$(window).bind("orientationchange resize pageshow", fixgeometry);
});

var addGoods = function() {
	var goodsName = $("#goodsName").val();
	var regionId = $("#regionId").val();
	if (!goodsName) {
		alert("商品名称不能为空!");
		return;
	}
	if (!regionId) {
		alert("区域ID不能为空!");
		return;
	}
	var params = {goodsName : $("#goodsName").val(), regionId : $("#regionId").val()};
	postAjax("http://wswangbo007.tpddns.cn:8002/teacher-corese-system-web/goodsController/addGoodsByParams",
			params,
			function(data) {
				if (data) {
					alert(data.message);
				}
			},
			function(data) {
				if (data) {
					alert(data.message);
				}
			});
}