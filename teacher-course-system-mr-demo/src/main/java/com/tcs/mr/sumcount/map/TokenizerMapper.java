package com.tcs.mr.sumcount.map;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TokenizerMapper extends Mapper<Object,Text,Text,IntWritable> {
	
	private final Logger logger = LoggerFactory.getLogger(TokenizerMapper.class);
	
	private final IntWritable one = new IntWritable(1);
	
	private final Text word = new Text();
	
	private String print = null;
	
	public void map(Object key,Text value, Context context) throws IOException, InterruptedException {
		StringTokenizer itr = new StringTokenizer(value.toString());
		while (itr.hasMoreTokens()) {
			print = itr.nextToken();
			logger.info(" itr.tostring : {} and value : {} " , print , value.toString());
			word.set(print);
			context.write(word, one);
		}
	}
}
