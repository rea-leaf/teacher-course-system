package com.tcs.mr.sumcount.reducer;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IntSumReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
	
	private final Logger logger = LoggerFactory.getLogger(IntSumReducer.class);
	
	private final IntWritable result = new IntWritable();
	
	@Override
	public void reduce(Text key, Iterable<IntWritable> values,Context context) throws IOException, InterruptedException {
		logger.info(" key : {} , values : {} , context : {} " , key, values, context);
		int sum = 0;
		for (IntWritable val : values) {
			sum += val.get();
		}
		result.set(sum);
		logger.info("key : {} , result : {} " , key , result);
		context.write(key, result);
	}
}