package com.tcs.model.monitoring.info;

import java.io.Serializable;

/**
 * 监控主题信息
 * @author wangbo
 */
public class MonitoringProperty implements Serializable {
	
	private static final long serialVersionUID = 1L;

	// 用户名
	private String userName;
	
	// 获取计算机名
	private String computerName;
	
	// IP
	private String ip;
	
	// 主机名
	private String HostName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getComputerName() {
		return computerName;
	}

	public void setComputerName(String computerName) {
		this.computerName = computerName;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getHostName() {
		return HostName;
	}

	public void setHostName(String hostName) {
		HostName = hostName;
	}

	public MonitoringProperty(String userName, String computerName, String ip, String hostName) {
		super();
		this.userName = userName;
		this.computerName = computerName;
		this.ip = ip;
		HostName = hostName;
	}

	public MonitoringProperty() {
		super();
	}

	@Override
	public String toString() {
		return "MonitoringProperty [userName=" + userName + ", computerName=" + computerName + ", ip=" + ip
				+ ", HostName=" + HostName + "]";
	}
	
}