package com.tcs.model.monitoring.info;

import java.io.Serializable;

/**
 * 监控内存
 * @author wangbo
 */
public class MonitoringMemory implements Serializable {
	
	private static final long serialVersionUID = 1L;

	// 内存总量
	private long memTotal;

	// 当前内存使用量
	private long memUsed;
	
	// 当前内存剩余量
	private long memFree;

	/**
	 * 内存总量
	 * @return
	 */
	public long getMemTotal() {
		return memTotal;
	}

	public void setMemTotal(long memTotal) {
		this.memTotal = memTotal;
	}

	/**
	 * 当前内存使用量
	 * @return
	 */
	public long getMemUsed() {
		return memUsed;
	}

	public void setMemUsed(long memUsed) {
		this.memUsed = memUsed;
	}

	/**
	 * 当前内存剩余量
	 * @return
	 */
	public long getMemFree() {
		return memFree;
	}

	public void setMemFree(long memFree) {
		this.memFree = memFree;
	}

	public MonitoringMemory(long memTotal, long memUsed, long memFree) {
		super();
		this.memTotal = memTotal;
		this.memUsed = memUsed;
		this.memFree = memFree;
	}

	public MonitoringMemory() {
		super();
	}

	@Override
	public String toString() {
		return "MonitoringMemory [memTotal=" + memTotal + ", memUsed=" + memUsed + ", memFree=" + memFree + "]";
	}
}