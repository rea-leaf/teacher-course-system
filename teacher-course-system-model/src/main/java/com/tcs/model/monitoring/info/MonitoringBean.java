package com.tcs.model.monitoring.info;

import java.util.List;

import com.tcs.model.base.BaseBean;

/**
 * 监控对象
 * @author wangbo
 */
public class MonitoringBean extends BaseBean {

	private static final long serialVersionUID = 1L;

	// IP
	protected String ip;
	
	// 监控进程详情Bean
	protected List<MonitoringDetalBean> detalBeanList;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public MonitoringBean(String ip, List<MonitoringDetalBean> detalBeanList) {
		super();
		this.ip = ip;
		this.detalBeanList = detalBeanList;
	}
	
	public MonitoringBean(String id,String ip, List<MonitoringDetalBean> detalBeanList) {
		this(ip,detalBeanList);
		this.id = id;
	}

	public MonitoringBean() {
		super();
	}

	public List<MonitoringDetalBean> getDetalBeanList() {
		return detalBeanList;
	}

	public void setDetalBeanList(List<MonitoringDetalBean> detalBeanList) {
		this.detalBeanList = detalBeanList;
	}

	@Override
	public String toString() {
		return "MonitoringBean [ip=" + ip + ", detalBeanList=" + detalBeanList + ", id=" + id + ", createDate="
				+ createDate + "]";
	}
}