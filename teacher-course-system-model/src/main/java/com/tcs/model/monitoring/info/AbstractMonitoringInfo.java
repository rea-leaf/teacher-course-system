package com.tcs.model.monitoring.info;

import java.io.Serializable;

/**
 * 抽象监控
 * 
 * @author wangbo
 */
public class AbstractMonitoringInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	// 系统信息
	protected MonitoringProperty monitoringProperty;

	// 提示信息
	protected String message;

	// 主题
	protected String subject;

	// 内容
	protected String content;

	public String getMessage() {
		return message;
	}

	public AbstractMonitoringInfo setMessage(String message) {
		this.message = message;
		return this;
	}

	public String getSubject() {
		return subject;
	}

	public AbstractMonitoringInfo setSubject(String subject) {
		this.subject = subject;
		return this;
	}

	public String getContent() {
		return content;
	}

	public AbstractMonitoringInfo setContent(String content) {
		this.content = content;
		return this;
	}

	public MonitoringProperty getMonitoringProperty() {
		return monitoringProperty;
	}

	public void setMonitoringProperty(MonitoringProperty monitoringProperty) {
		this.monitoringProperty = monitoringProperty;
	}
	
	
}