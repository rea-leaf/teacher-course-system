package com.tcs.model.monitoring.info;

public class MonitoringNetInfo extends AbstractMonitoringInfo {

	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "MonitoringNetInfo [message=" + message + ", subject=" + subject + ", content=" + content + "]";
	}
}