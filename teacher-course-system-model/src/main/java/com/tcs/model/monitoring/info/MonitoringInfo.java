package com.tcs.model.monitoring.info;

/**
 * 监控实体类
 * @author wangbo
 */
public class MonitoringInfo extends AbstractMonitoringInfo {

	private static final long serialVersionUID = 1L;
	
	// 监控内存信息
	private MonitoringMemory monitoringMemory;
	
	// 监控CPU情况
	private MonitoringCpu[] monitoringCpu;

	public MonitoringMemory getMonitoringMemory() {
		return monitoringMemory;
	}

	public void setMonitoringMemory(MonitoringMemory monitoringMemory) {
		this.monitoringMemory = monitoringMemory;
	}

	public MonitoringCpu[] getMonitoringCpu() {
		return monitoringCpu;
	}

	public void setMonitoringCpu(MonitoringCpu[] monitoringCpu) {
		this.monitoringCpu = monitoringCpu;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(200);
		sb.append("===================================基本信息===============================").append("\n")
		.append(" 主机名 : ").append(monitoringProperty.getHostName()).append("\n")
		.append("计算机名称 : ").append(monitoringProperty.getComputerName()).append("\n")
		.append("IP :").append(monitoringProperty.getIp()).append("\n")
		.append("用户名:").append(monitoringProperty.getUserName()).append("\n")
		.append("===================================内存===============================").append("\n")
		.append("总内存:").append(monitoringMemory.getMemTotal()).append("\n")
		.append("当前内存使用量:").append(monitoringMemory.getMemUsed()).append("\n")
		.append("当前内存剩余量:").append(monitoringMemory.getMemFree()).append("\n")
		.append("===================================CPU===============================").append("\n");
		for (MonitoringCpu cpu: monitoringCpu) {
			sb.append("用户使用率 : ").append(cpu.getCpuUser()).append("\n")
			.append("系统使用率 : ").append(cpu.getCpuSys()).append("\n")
			.append("当前等待率 : ").append(cpu.getCpuWait()).append("\n")
			.append("总的使用率 : ").append(cpu.getCpuCombined()).append("\n");
		}
		return sb.toString();
	}
}