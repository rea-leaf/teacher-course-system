package com.tcs.model.monitoring.info;

/**
 * 监控详情对象
 * 
 * @author wangbo
 *
 */
public class MonitoringDetalBean extends MonitoringBean {

	private static final long serialVersionUID = 1L;

	// 进程详情
	private String deatil;

	// 父ID
	private String spuerId;

	public String getSpuerId() {
		return spuerId;
	}

	public void setSpuerId(String spuerId) {
		this.spuerId = spuerId;
	}

	public String getDeatil() {
		return deatil;
	}

	public void setDeatil(String deatil) {
		this.deatil = deatil;
	}

	public MonitoringDetalBean(String deatil, String spuerId) {
		super();
		this.deatil = deatil;
		this.spuerId = spuerId;
	}
	
	public MonitoringDetalBean(String id,String ip,String deatil, String spuerId) {
		this(deatil,spuerId);
		this.id = id;
		this.deatil = deatil;
		this.ip = ip;
	}

	public MonitoringDetalBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "MonitoringDetalBean [deatil=" + deatil + ", spuerId=" + spuerId + ", ip=" + ip + ", detalBeanList="
				+ detalBeanList + ", id=" + id + ", createDate=" + createDate + "]";
	}
}