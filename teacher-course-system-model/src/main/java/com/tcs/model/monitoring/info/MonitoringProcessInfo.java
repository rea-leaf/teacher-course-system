package com.tcs.model.monitoring.info;

/**
 * 进程监控对象
 * @author wangbo
 */
public class MonitoringProcessInfo extends AbstractMonitoringInfo {

	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "MonitoringProcessInfo [monitoringProperty=" + monitoringProperty + ", message=" + message + ", subject="
				+ subject + ", content=" + content + "]";
	}
}