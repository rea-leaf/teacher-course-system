package com.tcs.model.monitoring.info;

import java.io.Serializable;

/**
 * 监控CPU
 * @author wangBo
 */
public class MonitoringCpu implements Serializable {
	
	private static final long serialVersionUID = 1L;

	// 用户使用率
	private double cpuUser;
	
	// 系统使用率
	private double cpuSys;

	// 当前等待率
	private double cpuWait;
	
	// 总的使用率
	private double cpuCombined;

	public double getCpuUser() {
		return cpuUser;
	}

	public void setCpuUser(double cpuUser) {
		this.cpuUser = cpuUser;
	}

	public double getCpuSys() {
		return cpuSys;
	}

	public void setCpuSys(double cpuSys) {
		this.cpuSys = cpuSys;
	}

	public double getCpuWait() {
		return cpuWait;
	}

	public void setCpuWait(double cpuWait) {
		this.cpuWait = cpuWait;
	}

	public double getCpuCombined() {
		return cpuCombined;
	}

	public void setCpuCombined(double cpuCombined) {
		this.cpuCombined = cpuCombined;
	}

	public MonitoringCpu(double cpuUser, double cpuSys, double cpuWait, double cpuCombined) {
		super();
		this.cpuUser = cpuUser;
		this.cpuSys = cpuSys;
		this.cpuWait = cpuWait;
		this.cpuCombined = cpuCombined;
	}

	public MonitoringCpu() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "MonitoringCpu [cpuUser=" + cpuUser + ", cpuSys=" + cpuSys + ", cpuWait=" + cpuWait + ", cpuCombined="
				+ cpuCombined + "]";
	}
}