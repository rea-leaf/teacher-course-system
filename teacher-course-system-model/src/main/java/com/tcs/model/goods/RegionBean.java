package com.tcs.model.goods;

import com.tcs.model.base.BaseBean;

/**
 * 区域Bean
 * @author wangbo
 */
public class RegionBean extends BaseBean {

	private static final long serialVersionUID = 1L;

	// 区域名称
	private String regionValue;

	public String getRegionValue() {
		return regionValue;
	}

	public void setRegionValue(String regionValue, String id) {
		this.regionValue = regionValue;
		this.id = id;
	}
	
	public RegionBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RegionBean(String regionValue,String id) {
		super();
		this.regionValue = regionValue;
		this.id = id;
	}

	@Override
	public String toString() {
		return "RegionBean [regionValue=" + regionValue + ", id=" + id + ", createDate=" + createDate + "]";
	}
	
	
}
