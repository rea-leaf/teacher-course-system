package com.tcs.model.goods;

import com.tcs.model.base.BaseBean;

/**
 * 货物Bean
 * @author wangbo
 */
public class GoodsBean extends BaseBean {

	private static final long serialVersionUID = 1L;

	// 货物名称
	private String goodsName;
	
	// 状态
	private int status;

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public GoodsBean(String goodsName, int status,String id) {
		super();
		super.id = id;
		this.goodsName = goodsName;
		this.status = status;
	}

	public GoodsBean() {
		super();
	}

	@Override
	public String toString() {
		return "GoodsBean [goodsName=" + goodsName + ", status=" + status + ", id=" + id + ", createDate=" + createDate
				+ "]";
	}
}