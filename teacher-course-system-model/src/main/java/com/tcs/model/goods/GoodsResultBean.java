package com.tcs.model.goods;

import java.io.Serializable;

/**
 * 返回值
 * @author wangbo
 */
public class GoodsResultBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	// ID
	private String id;
	
	// 商品名称
	private String goodsName;
	
	// 商品ID
	private String goodsId;
	
	// 状态
	private int status;
	
	// 信息
	private String message;
	
	public GoodsResultBean(String message) {
		this.message = message;
	}
	
	public GoodsResultBean() {super();};
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "GoodsResultBean [id=" + id + ", goodsName=" + goodsName + ", goodsId=" + goodsId + ", status=" + status
				+ ", message=" + message + "]";
	}
}