package com.tcs.model.goods;

import com.tcs.model.base.BaseBean;

/**
 * 区域货物
 * @author wangbo
 */
public class RegionGoodsBean extends BaseBean {

	private static final long serialVersionUID = 1L;
	
	// 货物ID
	private String goodId;
	
	// 区域ID
	private String regionId;
	
	// 状态
	private int status;

	public String getGoodId() {
		return goodId;
	}

	public void setGoodId(String goodId) {
		this.goodId = goodId;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public RegionGoodsBean(String goodId, String regionId,int status,String id) {
		super();
		this.goodId = goodId;
		this.regionId = regionId;
		this.id = id;
		this.status = status;
	}

	public RegionGoodsBean() {
		super();
	}
}