package com.tcs.model.goods;

import java.io.Serializable;

import com.tcs.model.base.BaseBean;

/**
 * 货物请求参数
 * @author wangbo
 */
public class GoodsParamsBean extends BaseBean implements Serializable {
	
	private static final long serialVersionUID = 1L;

	// 区域ID
	private String regionId;
	
	// 商品ID
	private String goodsId;
	
	// 开始时间
	private long beginTime;
	
	// 结束时间
	private long endTime;
	
	// 选择时间
	private String date;
	
	// 状态
	private int status;
	
	// 货物名称
	private String goodsName;
	
	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	public long getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(long beginTime) {
		this.beginTime = beginTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}
	
	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public GoodsParamsBean(String regionId, long beginTime, long endTime) {
		super();
		this.regionId = regionId;
		this.beginTime = beginTime;
		this.endTime = endTime;
	}
	
	public GoodsParamsBean(String regionId, long beginTime, long endTime,String goodsId) {
		super();
		this.regionId = regionId;
		this.beginTime = beginTime;
		this.endTime = endTime;
		this.goodsId = goodsId;
	}
	
	public GoodsParamsBean(String regionId, long beginTime, long endTime,String goodsId,int status) {
		super();
		this.regionId = regionId;
		this.beginTime = beginTime;
		this.endTime = endTime;
		this.goodsId = goodsId;
		this.status = status;
	}
	
	public GoodsParamsBean(String regionId, long beginTime, long endTime,String goodsId,int status, String id) {
		super(id);
		this.regionId = regionId;
		this.beginTime = beginTime;
		this.endTime = endTime;
		this.goodsId = goodsId;
		this.status = status;
	}

	public GoodsParamsBean() {
		super();
	}

	@Override
	public String toString() {
		return "GoodsParamsBean [regionId=" + regionId + ", goodsId=" + goodsId + ", beginTime=" + beginTime
				+ ", endTime=" + endTime + ", date=" + date + ", status=" + status + "]";
	}
}