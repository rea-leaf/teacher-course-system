package com.tcs.model.stock;

/**
 * 单选框
* @Title: StockCheckBean.java
* @Package com.tcs.model.stock
* @author 神经刀
* @date 2018年6月28日
* @version V1.0
 */
public class StockCheckBean {

	private Integer flag;
	
	private String emailId;
	
	private String ruleId;

	public StockCheckBean() {
		super();
	}

	public StockCheckBean(Integer flag, String emailId, String ruleId) {
		super();
		this.flag = flag;
		this.emailId = emailId;
		this.ruleId = ruleId;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getRuleId() {
		return ruleId;
	}

	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}

	@Override
	public String toString() {
		return "StockCheckBean [flag=" + flag + ", emailId=" + emailId + ", ruleId=" + ruleId + "]";
	}
}