package com.tcs.model.stock;

import java.io.Serializable;

/**
 * 返回查询结果集
* @Title: StockResultBean.java
* @Package com.tcs.model.stock
* @author 神经刀
* @date 2018年6月28日
* @version V1.0
 */
public class StockResultBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String userName;
	
	private String email;
	
	private StockCheckBean stockStockCheckBean;
	
	private StockCheckBean montirongCheckBean;

	public StockResultBean() {
		super();
	}

	public StockResultBean(String userName, String email, StockCheckBean stockStockCheckBean) {
		super();
		this.userName = userName;
		this.email = email;
		this.stockStockCheckBean = stockStockCheckBean;
	}

	public StockResultBean(String userName, String email, StockCheckBean stockStockCheckBean,
			StockCheckBean montirongCheckBean) {
		super();
		this.userName = userName;
		this.email = email;
		this.stockStockCheckBean = stockStockCheckBean;
		this.montirongCheckBean = montirongCheckBean;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public StockCheckBean getStockStockCheckBean() {
		return stockStockCheckBean;
	}

	public void setStockStockCheckBean(StockCheckBean stockStockCheckBean) {
		this.stockStockCheckBean = stockStockCheckBean;
	}

	public StockCheckBean getMontirongCheckBean() {
		return montirongCheckBean;
	}

	public void setMontirongCheckBean(StockCheckBean montirongCheckBean) {
		this.montirongCheckBean = montirongCheckBean;
	}

	@Override
	public String toString() {
		return "StockResultBean [userName=" + userName + ", email=" + email + ", stockStockCheckBean="
				+ stockStockCheckBean + ", montirongCheckBean=" + montirongCheckBean + "]";
	}
}