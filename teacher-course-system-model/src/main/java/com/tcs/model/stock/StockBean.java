package com.tcs.model.stock;

import com.tcs.model.base.BaseBean;

/**
 * 金融/股票
* @Title: StockBean.java
* @Package com.tcs.model.stock
* @author 神经刀
* @date 2018年4月9日
* @version V1.0
 */
public class StockBean extends BaseBean {

	private static final long serialVersionUID = 1L;
	
	// 标题
	private String title;
	
	// 内容
	private String content;
	
	// 爬虫ID
	private int maxId;
	
	public StockBean() {
		super();
	}

	public StockBean(String title, String content) {
		super();
		this.title = title;
		this.content = content;
	}
	
	public StockBean(String id,String title, String content) {
		super(id);
		this.title = title;
		this.content = content;
	}
	
	public StockBean(String id,String title, String content,int maxId) {
		super(id);
		this.title = title;
		this.content = content;
		this.maxId = maxId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getMaxId() {
		return maxId;
	}

	public void setMaxId(int maxId) {
		this.maxId = maxId;
	}
}