package com.tcs.model.rule.email.user;

import com.tcs.model.base.BaseBean;

/**
 * 邮箱帐号
* @Title: EmailUser.java
* @Package com.tcs.model.rule.email.user
* @author 神经刀
* @date 2018年4月12日
* @version V1.0
 */
public class EmailUserBean extends BaseBean {

	private static final long serialVersionUID = 1L;

	private String email;
	
	private String userName;
	
	private int status;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	public EmailUserBean(String id,String email, String userName, int status) {
		super(id);
		this.email = email;
		this.userName = userName;
		this.status = status;
	}

	public EmailUserBean(String email, String userName, int status) {
		super();
		this.email = email;
		this.userName = userName;
		this.status = status;
	}

	public EmailUserBean() {
		super();
	}

	public EmailUserBean(String id) {
		super(id);
	}

	@Override
	public String toString() {
		return "EmailUserBean [email=" + email + ", userName=" + userName + ", status=" + status + ", id=" + id
				+ ", createDate=" + createDate + "]";
	}
}