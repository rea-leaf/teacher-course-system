package com.tcs.model.rule;

import com.tcs.model.base.BaseBean;

/**
 * 权限角色
* @Title: RuleBean.java
* @Package com.tcs.model.rule
* @author 神经刀
* @date 2018年4月12日
* @version V1.0
 */
public class RuleBean extends BaseBean {

	private static final long serialVersionUID = 1L;

	private String name;
	
	private int status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public RuleBean() {
		super();
	}

	public RuleBean(String id) {
		super(id);
	}

	public RuleBean(String name, int status) {
		super();
		this.name = name;
		this.status = status;
	}
	
	public RuleBean(String id,String name, int status) {
		super(id);
		this.name = name;
		this.status = status;
	}

	@Override
	public String toString() {
		return "RuleBean [name=" + name + ", status=" + status + ", id=" + id + ", createDate=" + createDate + "]";
	}
}