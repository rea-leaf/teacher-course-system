package com.tcs.model.rule.email;

import com.tcs.model.base.BaseBean;

/**
 * 邮箱角色关联
* @Title: EmailRuleBean.java
* @Package com.tcs.model.rule.email
* @author 神经刀
* @date 2018年4月12日
* @version V1.0
 */
public class EmailRuleBean extends BaseBean {

	private static final long serialVersionUID = 1L;

	private String ruleId;
	
	private String emailId;
	
	private int status;

	public String getRuleId() {
		return ruleId;
	}

	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public EmailRuleBean() {
		super();
	}

	public EmailRuleBean(String id) {
		super(id);
	}

	public EmailRuleBean(String ruleId, String emailId, int status) {
		super();
		this.ruleId = ruleId;
		this.emailId = emailId;
		this.status = status;
	}
	
	public EmailRuleBean(String id,String ruleId, String emailId, int status) {
		super(id);
		this.ruleId = ruleId;
		this.emailId = emailId;
		this.status = status;
	}
}