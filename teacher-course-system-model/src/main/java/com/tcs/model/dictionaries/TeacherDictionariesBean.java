package com.tcs.model.dictionaries;

import com.tcs.model.base.BaseBean;

/**
 * 字典表
 * @author wangBo
 */
public class TeacherDictionariesBean extends BaseBean {
	
	private static final long serialVersionUID = 1L;
	
	private String id;
	// 字典名称
	private String dictionariesName;
	// 字典
	private String dictionariesDesc;
	
	private int dictionariesType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDictionariesName() {
		return dictionariesName;
	}

	public void setDictionariesName(String dictionariesName) {
		this.dictionariesName = dictionariesName;
	}

	public String getDictionariesDesc() {
		return dictionariesDesc;
	}

	public void setDictionariesDesc(String dictionariesDesc) {
		this.dictionariesDesc = dictionariesDesc;
	}

	public int getDictionariesType() {
		return dictionariesType;
	}

	public void setDictionariesType(int dictionariesType) {
		this.dictionariesType = dictionariesType;
	}

	public TeacherDictionariesBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TeacherDictionariesBean(String id, String dictionariesName, String dictionariesDesc, int dictionariesType) {
		super();
		this.id = id;
		this.dictionariesName = dictionariesName;
		this.dictionariesDesc = dictionariesDesc;
		this.dictionariesType = dictionariesType;
	}
	
	public TeacherDictionariesBean(int dictionariesType) {
		super();
		this.dictionariesType = dictionariesType;
	}

	@Override
	public String toString() {
		return "TeacherDictionariesBean [id=" + id + ", dictionariesName=" + dictionariesName + ", dictionariesDesc="
				+ dictionariesDesc + ", dictionariesType=" + dictionariesType + "]";
	}
}