package com.tcs.model.base;

import java.io.Serializable;
import java.util.Date;

/**
 * 顶级对象
 * @author wangbo
 */
public class BaseBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	// ID
	protected String id;
	
	protected int pageNum;		// 当前页数
	
	protected int pageSize;		// 数量

	// 时间戳
	protected final long createDate = new Date().getTime() / 1000;

	public final long getCreateDate() {
		return createDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public BaseBean(){};
	
	public BaseBean(String id) {
		super();
		this.id = id;
	}
	
	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	@Override
	public String toString() {
		return "BaseBean [id=" + id + ", createDate=" + createDate + "]";
	}
}