package com.tcs.model.upload;

import com.tcs.model.base.BaseBean;

/**
 * 上传Bean
 * @author wangBo
 */
public class UploadBean extends BaseBean {

	private static final long serialVersionUID = 1L;

	// ID
	private String id;
	
	// 素材名称
	private String teacherCoreseName;
	
	// 素材路径
	private String teacherCoresePath;
	
	// 素材类型
	private String teacherCoreseType;
	
	// 素材原名
	private String teacherCoreseOldname;
	
	// 素材大小
	private long teacherCoreseSize;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTeacherCoreseName() {
		return teacherCoreseName;
	}

	public void setTeacherCoreseName(String teacherCoreseName) {
		this.teacherCoreseName = teacherCoreseName;
	}

	public String getTeacherCoresePath() {
		return teacherCoresePath;
	}

	public void setTeacherCoresePath(String teacherCoresePath) {
		this.teacherCoresePath = teacherCoresePath;
	}

	public long getTeacherCoreseSize() {
		return teacherCoreseSize;
	}

	public void setTeacherCoreseSize(long teacherCoreseSize) {
		this.teacherCoreseSize = teacherCoreseSize;
	}

	public String getTeacherCoreseType() {
		return teacherCoreseType;
	}

	public void setTeacherCoreseType(String teacherCoreseType) {
		this.teacherCoreseType = teacherCoreseType;
	}

	public String getTeacherCoreseOldname() {
		return teacherCoreseOldname;
	}

	public void setTeacherCoreseOldname(String teacherCoreseOldname) {
		this.teacherCoreseOldname = teacherCoreseOldname;
	}
	
	public static class Builder {
		
		// ID
		private String id;
		
		// 素材名称
		private String teacherCoreseName;
		
		// 素材路径
		private String teacherCoresePath;
		
		// 素材类型
		private String teacherCoreseType;
		
		// 素材原名
		private String teacherCoreseOldname;
		
		// 素材大小
		private long teacherCoreseSize;
		
		public Builder(){super();};
		
		public Builder setId(String id) {
			this.id = id;
			return this;
		}

		public Builder setTeacherCoreseName(String teacherCoreseName) {
			this.teacherCoreseName = teacherCoreseName;
			return this;
		}

		public Builder setTeacherCoresePath(String teacherCoresePath) {
			this.teacherCoresePath = teacherCoresePath;
			return this;
		}

		public Builder setTeacherCoreseType(String teacherCoreseType) {
			this.teacherCoreseType = teacherCoreseType;
			return this;
		}

		public Builder setTeacherCoreseOldname(String teacherCoreseOldname) {
			this.teacherCoreseOldname = teacherCoreseOldname;
			return this;
		}

		public Builder setTeacherCoreseSize(long teacherCoreseSize) {
			this.teacherCoreseSize = teacherCoreseSize;
			return this;
		}
		
		public UploadBean builder() {
			return new UploadBean(this);
		}
	}
	
	private UploadBean(Builder builder) {
		this.id = builder.id;
		this.teacherCoreseName = builder.teacherCoreseName;
		this.teacherCoresePath = builder.teacherCoresePath;
		this.teacherCoreseOldname = builder.teacherCoreseOldname;
		this.teacherCoreseSize = builder.teacherCoreseSize;
		this.teacherCoreseType = builder.teacherCoreseType;
	}

	@Override
	public String toString() {
		return "UploadBean [id=" + id + ", teacherCoreseName=" + teacherCoreseName + ", teacherCoresePath="
				+ teacherCoresePath + ", teacherCoreseType=" + teacherCoreseType + ", teacherCoreseOldname="
				+ teacherCoreseOldname + ", teacherCoreseSize=" + teacherCoreseSize + ", createDate=" + createDate
				+ "]";
	}
}