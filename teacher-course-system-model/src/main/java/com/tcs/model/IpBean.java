package com.tcs.model;

public class IpBean {

	private String ip;	// IP
	
	private Integer port;	// prot

	public IpBean(String ip, Integer port) {
		super();
		this.ip = ip;
		this.port = port;
	}

	public IpBean() {
		super();
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	@Override
	public String toString() {
		return "IpBean [ip=" + ip + ", port=" + port + "]";
	}
}