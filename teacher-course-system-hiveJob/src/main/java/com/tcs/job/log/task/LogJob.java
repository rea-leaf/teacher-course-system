package com.tcs.job.log.task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcs.util.jdbc.JDBCUtils;

public class LogJob implements Runnable {
	
	private final Logger logger = LoggerFactory.getLogger(LogJob.class);
	
	Connection conn = null;
	
    PreparedStatement st = null;
    
    ResultSet rs = null;

	@Override
	public void run() {
		try {
			// 获取连接
			conn = JDBCUtils.getConnection();
			// 编写sql
			String sql = "insert into category values (?,?)";
			// 创建语句执行者
			st= conn.prepareStatement(sql);
			rs = st.executeQuery();
		} catch (SQLException e) {
			logger.error("" , e);
		}
        
	}
	
	

}
