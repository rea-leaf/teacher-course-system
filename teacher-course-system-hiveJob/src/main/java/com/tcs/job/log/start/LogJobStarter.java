package com.tcs.job.log.start;

import com.tcs.job.log.task.LogJob;

/**
 * JOB
 * @author wangbo
 * @Date 2018-4-21 14:58:19
 */
public class LogJobStarter {

	public static void main(String[] args) {
		LogJob logJob = new LogJob();
		new Thread(logJob).start();
	}
}