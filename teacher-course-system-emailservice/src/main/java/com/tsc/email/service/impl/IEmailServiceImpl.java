package com.tsc.email.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcs.model.email.MailSenderInfo;
import com.tcs.rpc.service.IEmailService;
import com.tsc.email.queue.QueueUtil;

/**
 * dubbo 接受数据
 * @author wangbo
 */
public class IEmailServiceImpl implements IEmailService {
	
	private final Logger logger = LoggerFactory.getLogger(IEmailServiceImpl.class);
	
	@Override
	public void send(MailSenderInfo mailSenderInfo) {
		logger.info(" 接收到数据 monitoringInfo : {} " , mailSenderInfo);
		try {
			if (!QueueUtil.SINGLEING.getQueue().offer(mailSenderInfo)) {
				QueueUtil.SINGLEING.getQueue().put(mailSenderInfo);
			}
		} catch (InterruptedException e) {
			logger.error("" , e);
		} finally {
			logger.debug(" 队列深度 : {}" , QueueUtil.SINGLEING.getQueue().size());
		}
	}

}