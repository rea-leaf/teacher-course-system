package com.tsc.email.queue;

import java.util.concurrent.LinkedBlockingQueue;

import com.tcs.model.email.MailSenderInfo;

/**
 * 队列工具
 * @author wangbo
 */
public enum QueueUtil {
	
	SINGLEING;
	
	private LinkedBlockingQueue<MailSenderInfo> queue = new LinkedBlockingQueue<MailSenderInfo>(2000);

	public LinkedBlockingQueue<MailSenderInfo> getQueue() {
		return queue;
	}

	public void setQueue(LinkedBlockingQueue<MailSenderInfo> queue) {
		this.queue = queue;
	}
	
	
}
