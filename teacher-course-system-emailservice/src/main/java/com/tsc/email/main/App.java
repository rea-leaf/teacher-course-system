package com.tsc.email.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

	private Logger logger = LoggerFactory.getLogger(App.class);
	
	private Object lock = new Object();
	
	public static void main(String[] args) {
		new App().start();
	}
	
	public void start() {
		try {
			synchronized (lock) {
				ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/app.xml");
				context.registerShutdownHook();
				lock.wait();
			}
		} catch (InterruptedException e) {
			logger.error(""  , e);
		}
	}
}