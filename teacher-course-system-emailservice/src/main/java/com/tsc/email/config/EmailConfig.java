package com.tsc.email.config;

public class EmailConfig {

	// 服务器名称
	private String hostname;
	
	// 用户地址
	private String address;
	
	// 密码
	private String password;
	
	// 编码格式
	private String charset = "UTF-8";

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}
}