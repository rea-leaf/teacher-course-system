package com.tcs.startup;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
* @Title: StartUp.java
* @Package com.tcs.startup
* @author 神经刀
* @date 2018年7月28日
* @version V1.0
 */
public class StartUp {
	
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ConfigurableApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring/proxypoll.xml");
		applicationContext.registerShutdownHook();
	}
}