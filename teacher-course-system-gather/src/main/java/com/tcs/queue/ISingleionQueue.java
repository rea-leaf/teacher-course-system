package com.tcs.queue;

public interface ISingleionQueue<T> {
	
	T poll();

	boolean put(T t);
	
	int size();
}
