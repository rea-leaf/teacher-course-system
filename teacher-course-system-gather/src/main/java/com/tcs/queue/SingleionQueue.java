package com.tcs.queue;

import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component(value="singleionQueue")
public class SingleionQueue<T> implements ISingleionQueue<T> {
	
	private final Logger logger = LoggerFactory.getLogger(SingleionQueue.class);
	
	public LinkedBlockingQueue<T> dataQueue = new LinkedBlockingQueue<T>(500);

	@Override
	public T poll() {
		T t = null;
		try {
			t = dataQueue.take();
		} catch (InterruptedException e) {
			logger.error("" , e);
		}
		return t;
	}

	@Override
	public boolean put(T t) {
		try {
			dataQueue.put(t);
		} catch (InterruptedException e) {
			logger.error("" , e);
		}
		return false;
	}
	
	@Override
	public int size() {
		return dataQueue.size();
	}

}