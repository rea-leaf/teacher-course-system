package com.tcs.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
	
	private final Logger logger = LoggerFactory.getLogger(App.class);
	
	private Object lock = new Object();
	
	public static void main(String [] args) {
		new App().run();
	}

	private void run() {
		synchronized (lock) {
			ClassPathXmlApplicationContext app = new ClassPathXmlApplicationContext("classpath:spring/app.xml");
			try {
				app.registerShutdownHook();
				lock.wait();
			} catch (InterruptedException e) {
				logger.error(" 锁失败!" , e);
			}
		}
	}
}