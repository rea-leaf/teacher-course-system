package com.tcs.data.input;

/**
 * 数据接受接口
 * @author wangbo
 */
public interface IInputData {
	
	/**
	 * 接受数据
	 * @return
	 */
	String readSystemData();
}