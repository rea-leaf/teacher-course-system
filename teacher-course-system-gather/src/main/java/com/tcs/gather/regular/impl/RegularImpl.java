package com.tcs.gather.regular.impl;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.tcs.gather.model.CommandModel;
import com.tcs.gather.regular.IRegular;
import com.tcs.util.constant.SystemConstants;

/**
* @Title: RegularImpl.java
* @Package com.tcs.gather.regular.impl
* @author 神经刀
* @date 2018年3月27日
* @version V1.0
 */
@Component(value="regular")
public class RegularImpl<T> implements IRegular<T> {
	
	private final Logger logger = LoggerFactory.getLogger(RegularImpl.class);
	
	private String regularStr;		// 正则
	
	private boolean regularFalg = true;

	public RegularImpl() {};
	
	@Override
	public T regular(T t) {
		return t;
	}
	
	public boolean setRegular(CommandModel commandModel) {
		try {
			regularStr = commandModel.getProperties().getProperty(SystemConstants.GatherConstants.REGULAR);
			if (StringUtils.isBlank(regularStr)) {
				regularFalg = false;
			}
		} catch (Exception e) {
			logger.error("" , e);
			regularFalg = false;
		}
		return regularFalg;
	}
	
	@Override
	public boolean getRegular() {
		return regularFalg;
	}

}