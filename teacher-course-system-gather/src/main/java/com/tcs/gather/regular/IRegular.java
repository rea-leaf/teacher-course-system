package com.tcs.gather.regular;

import com.tcs.gather.model.CommandModel;

/**
 * 正则表达式
* @Title: IRegular.java
* @Package com.tcs.gather.regular
* @author 神经刀
* @date 2018年3月27日
* @version V1.0
 */
public interface IRegular<T> {

	/**
	* @Title: regular  
	* @Description: 正则表达式
	* @param @param t
	* @return T    返回类型  
	 */
	T regular(T t);
	
	/**
	* @Title: setRegular  
	* @Description: 正则启用
	* @param @param commandModel
	* @param @return    参数  
	* @return boolean    返回类型  
	* @throws
	 */
	boolean setRegular(CommandModel commandModel);
	
	/**
	* @Title: setRegular  
	* @Description: 正则启用
	* @param @param commandModel
	* @param @return    参数  
	* @return boolean    返回类型  
	* @throws
	 */
	boolean getRegular();
}
