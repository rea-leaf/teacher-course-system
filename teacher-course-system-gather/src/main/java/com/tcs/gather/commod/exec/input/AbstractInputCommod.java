package com.tcs.gather.commod.exec.input;

import javax.annotation.Resource;

import com.tcs.gather.commod.exec.ICommand;
import com.tcs.gather.model.CommandModel;
import com.tcs.gather.regular.IRegular;
import com.tcs.queue.ISingleionQueue;

/**
 * 执行总线
* @Title: AbstractInputCommod.java
* @Package com.tcs.gather.commod
* @author 神经刀
* @date 2018年3月21日
* @version V1.0
 */
public abstract class AbstractInputCommod implements ICommand {
	
	protected CommandModel commandModel;
	
	protected ISingleionQueue<String> queue;
	
	@Resource(name="regular")
	protected IRegular<String> regular;
	
	public AbstractInputCommod() {};
	
	public AbstractInputCommod(CommandModel commandModel , ISingleionQueue<String> queue) {
		this.commandModel = commandModel;
		this.queue = queue;
	}
	
	@Override
	public void exec(CommandModel commandModel) {
		this.execInputCommod(this.commandModel);
	}

	/**
	 * 执行计划
	* @Title: execInputCommod
	* @Description: 执行计划
	* @param CommandModel 参数
	* @return void
	 */
	public abstract void execInputCommod(CommandModel commandModel);
	
	/**
	* @Title: affer
	* @Description: 推入消息
	* @param message {@link String}
	* @return {@link Boolean}
	 */
	public boolean affer(String message) {
		return queue.put(message);
	}
	
	@Override
	public void run() {
		this.exec(commandModel);
	}
}