package com.tcs.gather.commod.exec.out;

import com.tcs.gather.commod.exec.ICommand;
import com.tcs.gather.model.CommandModel;
import com.tcs.queue.ISingleionQueue;

/**
 * 执行总线
* @Title: AbstractInputCommod.java
* @Package com.tcs.gather.commod
* @author 神经刀
* @date 2018年3月21日
* @version V1.0
 */
public abstract class AbstractOutCommod implements ICommand {
	
	protected CommandModel commandModel;
	
	protected ISingleionQueue<String> queue;
	
	public AbstractOutCommod() {};
	
	public AbstractOutCommod(CommandModel commandModel,ISingleionQueue<String> queue) {
		this.commandModel = commandModel;
		this.queue = queue;
	}
	
	@Override
	public void exec(CommandModel commandModel) {
		this.execOutCommod(this.commandModel);
	}

	/**
	 * 执行计划
	* @Title: execInputCommod
	* @Description: 执行计划
	* @param CommandModel 参数
	* @return void
	 */
	public abstract void execOutCommod(CommandModel commandModel);
	
	/**
	* @Title: poll  
	* @Description: 拉取消息
	* @return {@link String}
	* @throws InterruptedException
	 */
	public String poll() throws InterruptedException {
		return queue.poll();
	}
	
	@Override
	public void run() {
		this.exec(commandModel);
	}
	
}