package com.tcs.gather.commod.exec.out.impl;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcs.gather.commod.exec.out.AbstractOutCommod;
import com.tcs.gather.model.CommandModel;
import com.tcs.gather.model.kafka.KafkaCommandModel;
import com.tcs.queue.ISingleionQueue;
import com.tcs.util.constant.SystemConstants;

/**
 * @Title: InputLoggerCommod.java
 * @Package com.tcs.gather.commod.exec.input.impl
 * @author 神经刀
 * @date 2018年3月21日
 * @version V1.0
 */
public class KafkaOutCommod extends AbstractOutCommod {

	private final Logger logger = LoggerFactory.getLogger(KafkaOutCommod.class);
	
	private Producer<String, String> producer = null;
	
	public KafkaOutCommod() {};
	
	public KafkaOutCommod(CommandModel commandModel,ISingleionQueue<String> queue) {
		super(commandModel,queue);
	}

	@Override
	public void execOutCommod(CommandModel commandModel) {
		KafkaCommandModel kafkaCommandModel = (KafkaCommandModel)commandModel;
		producer = new KafkaProducer<>(kafkaCommandModel.getProducerProperties());
		String topic = commandModel.getProperties().getProperty(SystemConstants.GatherConstants.KAFKA_TOPIC);
		int partition = NumberUtils.toInt(commandModel.getProperties().getProperty(SystemConstants.GatherConstants.KAFKA_PARTITION));
		ProducerRecord<String,String> record = null;
		if (StringUtils.isBlank(topic)) {
			logger.error("" ,new NullPointerException("topic 为空!"));
			return;
		}
		String message = null;
		while (true) {
			try {
				logger.info(" kafka size : {}" , queue.size());
				message = queue.poll();
				Thread.sleep(1);
				record = new ProducerRecord<String, String>(topic, partition, String.valueOf(new Date().getTime()), message);
				logger.info(" topic : {} , partiton : {} , value : {}" , record.topic() , record.partition() , record.value());
				producer.send(record);
			} catch (Exception e) {
				logger.error("" , e);
			}
		}
	}
}