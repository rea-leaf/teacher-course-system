package com.tcs.gather.commod;

import java.util.concurrent.ExecutorService;

import com.tcs.gather.model.InputAndOutModel;
import com.tcs.queue.ISingleionQueue;

public interface ICommodExec {

	/**
	 * 执行命令
	 * @param InputAndOutModel
	 */
	public void execCommod(InputAndOutModel inputAndOutModel);
	
	/**
	 * 执行命令
	 * @param InputAndOutModel
	 * @param ExecutorService
	 */
	public void execCommod(InputAndOutModel inputAndOutModel,ExecutorService executor);
	
	/**
	 * 执行命令
	 * @param <T>
	 * @param InputAndOutModel
	 * @param ExecutorService
	 */
	public void execCommod(InputAndOutModel inputAndOutModel,ExecutorService executor,ISingleionQueue<String> queue);
}