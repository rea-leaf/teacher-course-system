package com.tcs.gather.commod.exec;

import com.tcs.gather.model.CommandModel;

/**
* @Title: IInputCommod.java
* @Package com.tcs.gather.commod.exec.input
* @author 神经刀
* @date 2018年3月21日
* @version V1.0
 */
public interface ICommand extends Runnable{

	/**
	* @Title: exec  
	* @Description: 执行计划
	* @param @param commandModel    参数  
	* @return void    返回类型  
	* @throws
	 */
	void exec(CommandModel commandModel);
}