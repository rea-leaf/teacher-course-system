package com.tcs.gather.commod.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.tcs.gather.commod.exec.out.impl.KafkaOutCommod;
import com.tcs.gather.model.InputAndOutModel;
import com.tcs.queue.ISingleionQueue;
import com.tcs.util.constant.SystemConstants;

/**
 * 输出
* @Title: OutCommodExecImpl.java
* @Package com.tcs.gather.commod.impl
* @author 神经刀
* @date 2018年3月21日
* @version V1.0
 */
@Component(value="outputCommodExec")
public class OutCommodExecImpl extends AbstractCommodExec {
	
	private final Logger logger = LoggerFactory.getLogger(OutCommodExecImpl.class);
	
	@Override
	protected void getCommand(InputAndOutModel inputAndOutModel) {
		logger.info(" commandModel 输出 : {}  " , inputAndOutModel.getOutCommandModel());
		String type = inputAndOutModel.getOutCommandModel().getProperties().getProperty(SystemConstants.GatherConstants.OUTPUTTYPE);
		if (type.equals(SystemConstants.GatherConstants.AKFKA)) {
//			inputCommod = new KafkaOutCommod(inputAndOutModel.getOutCommandModel());
		}
	}
	
	@Override
	protected void getCommand(InputAndOutModel inputAndOutModel, ISingleionQueue<String> queue) {
		logger.info(" commandModel 输出 : {}  " , inputAndOutModel.getOutCommandModel());
		String type = inputAndOutModel.getOutCommandModel().getProperties().getProperty(SystemConstants.GatherConstants.OUTPUTTYPE);
		if (type.equals(SystemConstants.GatherConstants.AKFKA)) {
			inputCommod = new KafkaOutCommod(inputAndOutModel.getOutCommandModel() , queue);
		}
	}
}