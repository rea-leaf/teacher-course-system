package com.tcs.gather.commod.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.tcs.gather.commod.exec.input.impl.InputLoggerCommod;
import com.tcs.gather.model.InputAndOutModel;
import com.tcs.queue.ISingleionQueue;
import com.tcs.util.constant.SystemConstants;

/**
 * 执行命令
 * @author wangbo
 */
@Component(value="inputCommodExec")
public class InputCommodExecImpl extends AbstractCommodExec {
	
	private final Logger logger = LoggerFactory.getLogger(InputCommodExecImpl.class);
	
	@Override
	protected void getCommand(InputAndOutModel inputAndOutModel) {
		logger.info(" commandModel : {} " , inputAndOutModel.getInputCommandModel());
		String type = inputAndOutModel.getInputCommandModel().getProperties().getProperty(SystemConstants.GatherConstants.INPUTTYPE);
		// logger
		if (type.equals(SystemConstants.GatherConstants.LOGGER)) {
//			inputCommod = new InputLoggerCommod(inputAndOutModel.getInputCommandModel());
		}
	}
	
	@Override
	protected void getCommand(InputAndOutModel inputAndOutModel, ISingleionQueue<String> queue) {
		logger.info(" commandModel : {} " , inputAndOutModel.getInputCommandModel());
		String type = inputAndOutModel.getInputCommandModel().getProperties().getProperty(SystemConstants.GatherConstants.INPUTTYPE);
		// logger
		if (type.equals(SystemConstants.GatherConstants.LOGGER)) {
			inputCommod = new InputLoggerCommod(inputAndOutModel.getInputCommandModel(),queue);
		}
	}
}