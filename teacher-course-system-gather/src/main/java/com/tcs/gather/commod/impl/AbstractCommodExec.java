package com.tcs.gather.commod.impl;

import java.util.concurrent.ExecutorService;

import com.tcs.gather.commod.ICommodExec;
import com.tcs.gather.commod.exec.ICommand;
import com.tcs.gather.model.InputAndOutModel;
import com.tcs.queue.ISingleionQueue;

public abstract class AbstractCommodExec implements ICommodExec {
	
	protected ExecutorService executor;
	
	public AbstractCommodExec() {};
	
	public AbstractCommodExec(ExecutorService executor) {
		this.executor = executor;
	}
	
	protected ICommand inputCommod;
	
	protected void execute() {
		executor.execute(inputCommod);
	}
	
	@Override
	public void execCommod(InputAndOutModel inputAndOutModel) {
		getCommand(inputAndOutModel);
		execute();
	}
	
	@Override
	public void execCommod(InputAndOutModel inputAndOutModel, ExecutorService executor) {
		this.executor = executor;
		execCommod(inputAndOutModel);
	}
	
	@Override
	public void execCommod(InputAndOutModel inputAndOutModel, ExecutorService executor, ISingleionQueue<String> queue) {
		this.executor = executor;
		getCommand(inputAndOutModel, queue);
		execute();
	}

	protected abstract void getCommand(InputAndOutModel inputAndOutModel);
	
	protected abstract void getCommand(InputAndOutModel inputAndOutModel , ISingleionQueue<String> queue);

}
