package com.tcs.gather.model;

import java.util.ArrayList;
import java.util.List;

import com.tcs.gather.commod.ICommodExec;

/**
* @Title: InputAndOutModel.java
* @Package com.tcs.gather.model
* @author 神经刀
* @date 2018年3月26日
* @version V1.0
 */
public class InputAndOutModel {
	
	// input
	private CommandModel inputCommandModel;
	
	// out
	private CommandModel outCommandModel;
	
	private List<ICommodExec> commodList = new ArrayList<ICommodExec>(2);

	public CommandModel getInputCommandModel() {
		return inputCommandModel;
	}

	public void setInputCommandModel(CommandModel inputCommandModel) {
		this.inputCommandModel = inputCommandModel;
	}

	public CommandModel getOutCommandModel() {
		return outCommandModel;
	}

	public void setOutCommandModel(CommandModel outCommandModel) {
		this.outCommandModel = outCommandModel;
	}
	
	public List<ICommodExec> getCommodList() {
		return commodList;
	}

	public void addCommodExec(ICommodExec iCommodExec) {
		this.commodList.add(iCommodExec);
	}

	@Override
	public String toString() {
		return "InputAndOutModel [inputCommandModel=" + inputCommandModel + ", outCommandModel=" + outCommandModel
				+ ", commodList=" + commodList + "]";
	}
}