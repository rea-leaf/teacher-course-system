package com.tcs.gather.model;

import java.io.Serializable;
import java.util.Properties;

/**
 * 数据模型
 * @author wangbo
 */
public abstract class CommandModel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	protected Properties properties;
	
	public CommandModel() {};
	
	public CommandModel(Properties properties) {
		this.properties = properties;
		checkParams();
	}
	
	/**
	* @Title: checkParams  
	* @Description: 检查
	 */
	public abstract void checkParams() throws NullPointerException;

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}
}