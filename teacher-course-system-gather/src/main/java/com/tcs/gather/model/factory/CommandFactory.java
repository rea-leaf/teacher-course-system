package com.tcs.gather.model.factory;

import java.util.Properties;

import com.tcs.gather.model.InputAndOutModel;
import com.tcs.gather.model.kafka.KafkaCommandModel;
import com.tcs.gather.model.logger.LoggerCommandModel;
import com.tcs.util.constant.SystemConstants;

public class CommandFactory {

	private CommandFactory() {};
	
	public static InputAndOutModel createInputAndOut(Properties properties) {
		InputAndOutModel inputAndOutModel = new InputAndOutModel();
		if (properties.containsKey(SystemConstants.GatherConstants.INPUTTYPE) && properties.getProperty(SystemConstants.GatherConstants.INPUTTYPE).equals(SystemConstants.GatherConstants.LOGGER)) {
			// log
			inputAndOutModel.setInputCommandModel(new LoggerCommandModel(properties));
		} 
		if (properties.containsKey(SystemConstants.GatherConstants.INPUTTYPE) && properties.getProperty(SystemConstants.GatherConstants.INPUTTYPE).equals(SystemConstants.GatherConstants.AKFKA)) {
			// kafka 消费者
			inputAndOutModel.setInputCommandModel(new KafkaCommandModel(properties));
		}
		if (properties.containsKey(SystemConstants.GatherConstants.OUTPUTTYPE) && properties.getProperty(SystemConstants.GatherConstants.OUTPUTTYPE).equals(SystemConstants.GatherConstants.AKFKA)) {
			// kafka 生产者
			inputAndOutModel.setOutCommandModel(new KafkaCommandModel(properties));
		}
		return inputAndOutModel;
	}
}
