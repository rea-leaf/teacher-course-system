package com.tcs.gather.model.logger;

import java.util.Properties;

import org.apache.commons.lang.StringUtils;

import com.tcs.gather.model.CommandModel;
import com.tcs.util.constant.SystemConstants;

/**
 * LOGGER
* @Title: LoggerCommandModel.java
* @Package com.tcs.gather.model.logger
* @author 神经刀
* @date 2018年3月26日
* @version V1.0
 */
public class LoggerCommandModel extends CommandModel {

	private static final long serialVersionUID = 1L;

	// 输入端路径
	protected String inputPath;

	// 输入文件
	protected String inputFile;

	// 输入命令
	protected String inputCommod;

	// 输入类型
	protected String inputType;

	public LoggerCommandModel(Properties properties) {
		super(properties);
	}

	@Override
	public void checkParams() throws NullPointerException{
		if (properties.getProperty(SystemConstants.GatherConstants.INPUTTYPE).equals("logger")) {
			if (StringUtils.isBlank(properties.getProperty(SystemConstants.GatherConstants.INPUTPATH))) {
				throw new NullPointerException(SystemConstants.GatherConstants.INPUTPATH + "为空!");
			} else if (StringUtils.isBlank(properties.getProperty(SystemConstants.GatherConstants.INPUTFILE))) {
				throw new NullPointerException(SystemConstants.GatherConstants.INPUTFILE + "为空!");
			} else if (StringUtils.isBlank(properties.getProperty(SystemConstants.GatherConstants.INPUTCOMMOD))) {
				throw new NullPointerException(SystemConstants.GatherConstants.INPUTCOMMOD + "为空!");
			}
		}
		inputPath = StringUtils.isNotBlank(properties.getProperty(SystemConstants.GatherConstants.INPUTPATH)) ? properties.getProperty(SystemConstants.GatherConstants.INPUTPATH) : "";
		inputFile = StringUtils.isNotBlank(properties.getProperty(SystemConstants.GatherConstants.INPUTFILE)) ? properties.getProperty(SystemConstants.GatherConstants.INPUTFILE) : "";
		inputCommod = StringUtils.isNotBlank(properties.getProperty(SystemConstants.GatherConstants.INPUTCOMMOD)) ? properties.getProperty(SystemConstants.GatherConstants.INPUTCOMMOD) : "";
		inputType = StringUtils.isNotBlank(properties.getProperty(SystemConstants.GatherConstants.INPUTTYPE)) ? properties.getProperty(SystemConstants.GatherConstants.INPUTTYPE) : "";
	}

	public String getInputPath() {
		return inputPath;
	}

	public void setInputPath(String inputPath) {
		this.inputPath = inputPath;
	}

	public String getInputFile() {
		return inputFile;
	}

	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}

	public String getInputCommod() {
		return inputCommod;
	}

	public void setInputCommod(String inputCommod) {
		this.inputCommod = inputCommod;
	}

	public String getInputType() {
		return inputType;
	}

	public void setInputType(String inputType) {
		this.inputType = inputType;
	}

}