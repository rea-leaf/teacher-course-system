package com.tcs.gather.config;

import java.io.InputStream;
import java.util.Properties;

import com.tcs.gather.model.CommandModel;

/**
 * 配置解析
 * @author wangbo
 *
 */
public interface IConfig {
	
	/**
	 * 获取配置文件
	 * @param configModel : {@link ConfigModel}
	 * @return {@link InputStream}
	 */
	InputStream getFile(ConfigModel configModel);
	
	/**
	 * 获取jdk配置
	 * @param inputStream {@link InputStream}
	 * @return {@link Properties}
	 */
	Properties readFile(InputStream inputStream);
	
	/**
	 * 创建配置
	 * @param properties {@link Properties}
	 * @return {@link CommandModel}
	 */
	CommandModel createCommandModel(Properties properties);
}