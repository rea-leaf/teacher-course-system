package com.tcs.gather.config;

import java.io.Serializable;

import com.tcs.util.constant.SystemConstants;

public class ConfigModel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	// ConfigKey
	public String ConfigKey = SystemConstants.GatherConstants.CONFIGFILE;

	// 文件路径
	private String filePath;

	public String getFilePath() {
		filePath = System.getProperty(this.ConfigKey);
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public ConfigModel(String filePath) {
		super();
		this.filePath = filePath;
	}

	public ConfigModel() {
		super();
	}
}