package com.tcs.gather.config.check.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.tcs.gather.config.ConfigModel;
import com.tcs.gather.config.check.ICheckConfig;
import com.tcs.util.system.exception.GatherFileNotExistsException;

/**
 * 效验数据是否合法
 * @author wangbo
 */
@Component(value = "checkConfig")
public class CheckConfigImpl implements ICheckConfig {

	@Override
	public boolean checkFilePath(ConfigModel configModel) throws GatherFileNotExistsException {
		boolean result = false;
		if (null != configModel) {
			if (StringUtils.isNotBlank(configModel.getFilePath())) {
				result = true;
			} else {
				throw new GatherFileNotExistsException("未能发现配置文件");
			}
		}
		return result;
	}
}