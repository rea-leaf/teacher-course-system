package com.tcs.gather.config.check;

import com.tcs.gather.config.ConfigModel;
import com.tcs.util.system.exception.GatherFileNotExistsException;

/**
 * 效验配置
 * @author wangbo
 */
public interface ICheckConfig {

	/**
	 * 效验文件路径
	 * @param configModel : {@link ConfigModel}
	 * @return {@link Boolean}
	 * @throws GatherFileNotExistsException : {@link GatherFileNotExistsException}
	 */
	boolean checkFilePath(ConfigModel configModel) throws GatherFileNotExistsException;
}