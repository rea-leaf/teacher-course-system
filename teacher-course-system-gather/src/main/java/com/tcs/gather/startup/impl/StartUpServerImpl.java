package com.tcs.gather.startup.impl;


import java.util.Properties;

import javax.annotation.Resource;

import com.tcs.gather.config.ConfigModel;
import com.tcs.gather.config.IConfig;
import com.tcs.gather.startup.IStartUpServer;

public class StartUpServerImpl implements IStartUpServer {
	
	@Resource(name="config")
	private IConfig config;
	
	@Override
	public void init() {
		Properties properties = config.readFile(config.getFile(new ConfigModel()));
		config.createCommandModel(properties);
	}

	@Override
	public void destroy() {}

	public void setConfig(IConfig config) {
		this.config = config;
	}
}	