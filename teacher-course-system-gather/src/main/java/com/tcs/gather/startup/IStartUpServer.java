package com.tcs.gather.startup;

/**
 * 启动接口
 * @author wangbo
 *
 */
public interface IStartUpServer {

	/**
	 * 初始化
	 */
	void init();
	
	/**
	 * 销毁
	 */
	void destroy();
}
