package com.tcs.rpc.service;

import com.tcs.model.email.MailSenderInfo;

/**
 * 邮箱服务
 * @author wangBo
 */
public interface IEmailService {

	/**
	 * 发送邮箱
	 * @param monitoringInfo : 监控对象
	 */
	public void send(MailSenderInfo mailSenderInfo);
	
}