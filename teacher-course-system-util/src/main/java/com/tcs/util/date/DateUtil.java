package com.tcs.util.date;

import java.util.Date;

import org.joda.time.DateTime;

/**
 * 日期工具类
 * 
 * @author wangbo
 *
 */
public class DateUtil {

	private DateUtil() {
		super();
	};

	private static final String FORMATE_DATE = "yyyy-MM-dd";
	private static final String FORMATE_SECONDS = "HH:mm:ss";
	@SuppressWarnings("unused")
	private static final String FORMATE_FULL = FORMATE_DATE.concat(" ").concat(FORMATE_SECONDS);
	
	/**
	 * 获取当天的开始时间戳
	 * @return {@link Long}
	 */
	public static long getBeginOfToday() {
		DateTime dateTime = new DateTime(new Date());
		return dateTime.withTimeAtStartOfDay().getMillis();
	}
	
	/**
	 * 获取当天的结束时间戳
	 * @return
	 */
	public static long getEndOfToday() {
		DateTime dateTime = new DateTime(new Date());
		return dateTime.millisOfDay().withMaximumValue().getMillis();
	}
	
	/**
	 * 获取当天的开始时间戳
	 * @return {@link Long}
	 */
	public static long getBeginByStr(String date) {
		DateTime dateTime = new DateTime(date);
		return dateTime.withTimeAtStartOfDay().getMillis();
	}
	
	/**
	 * 获取当天的结束时间戳
	 * @return
	 */
	public static long getEndByStr(String date) {
		DateTime dateTime = new DateTime(date);
		return dateTime.millisOfDay().withMaximumValue().getMillis();
	}
}