package com.tcs.util.ip;

import org.springframework.stereotype.Component;

@Component(value="ipUtil")
public class IpUtil {

	/**
	 * 
	* @Title: getIp  
	* @Description: 获取IP地址
	* @param @param request
	* @param @return    参数  
	* @return String    返回类型  
	 */
//	public String getIp(HttpServletRequest request) { 
//		//		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
//		String ip = request.getHeader("X-Forwarded-For");
//		if (StringUtils.isNotEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)) {
//			// 多次反向代理后会有多个ip值，第一个ip才是真实ip
//			int index = ip.indexOf(",");
//			if (index != -1) {
//				return ip.substring(0, index);
//			} else {
//				return ip;
//			}
//		}
//		ip = request.getHeader("X-Real-IP");
//		if (StringUtils.isNotEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)) {
//			return ip;
//		}
//		return request.getRemoteAddr();
//	}
}