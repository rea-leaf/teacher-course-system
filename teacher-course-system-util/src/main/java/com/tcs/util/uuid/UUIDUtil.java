package com.tcs.util.uuid;

import java.util.UUID;

/**
 * UUID
 * @author wangBo
 */
public class UUIDUtil {
	
	private UUIDUtil(){super();};
	
	public static String getUUID() {
		return UUID.randomUUID().toString().replace("-", "");
	}
}
