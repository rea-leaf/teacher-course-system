package com.tcs.util.spring;

import java.util.concurrent.locks.ReentrantLock;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * spring 容器工具
 * @author wangbo
 */
public class SpringConfigTool implements ApplicationContextAware {
	
	private static ApplicationContext context = null;
    private static SpringConfigTool stools = null;
    
    private static ReentrantLock lock = new ReentrantLock();
    
    private SpringConfigTool(){};

    public synchronized static SpringConfigTool init() {
        if (stools == null) {
            stools = new SpringConfigTool();
        }
        return stools;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext)
            throws BeansException {
        context = applicationContext;
    }

    public static Object getBean(String beanName) throws BeansException{
    	Object resultObj = null;
    	try {
			lock.lock();
			resultObj = context.getBean(beanName);
		} catch (BeansException e) {
			throw e;
		} finally {
			lock.unlock();
		}
        return resultObj;
    }
}