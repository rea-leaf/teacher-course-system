package com.tcs.util.zip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * ZIP 工具类
 * @author wangBo
 */
public class ZipUtil {
	
	private ZipUtil(){super();};
	
	private static final int size = 1024 * 1024 * 1;
	
	/**
	 * 压缩文件
	 * 
	 * @param srcfile
	 *            File[] 需要压缩的文件列表 "F:\\asd"
	 * @param zipfile
	 *            File 压缩后的文件 "f:\\qwe_16_V6.0.0_1.zip";
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static void ZipFiles(java.io.File[] srcfile, java.io.File zipfile) throws IOException {
		ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(zipfile));
		for (int i = 0; i < srcfile.length; ++i) {
			if (srcfile[i].isDirectory()) {
				zipDir(zipOut, srcfile[i], srcfile[i].getName());
			} else {
				zipFile(zipOut, srcfile[i], null);
			}
		}
		zipOut.close();
	}

	/**
	 * 递归压缩文件夹 faterName的目的就是为了维持原来的目录
	 * 
	 * @author hulujie
	 * @since 2016年9月2日 下午5:33:26
	 * @param zipOut
	 * @param dirFile
	 * @param faterName
	 * @return
	 * @throws IOException
	 */
	private static ZipOutputStream zipDir(ZipOutputStream zipOut, File dirFile, String faterName) throws IOException {
		File fileList[] = dirFile.listFiles();
		for (int i = 0; i < fileList.length; i++) {
			if (fileList[i].isDirectory()) {
				zipDir(zipOut, fileList[i], faterName + File.separator + fileList[i].getName());
			} else {
				zipFile(zipOut, fileList[i], faterName);
			}
		}
		return zipOut;
	}

	/**
	 * 压缩单个文件
	 * 
	 * @author hulujie
	 * @since 2016年9月2日 下午5:40:26
	 * @param zipOut
	 * @param file
	 * @throws IOException
	 */
	private static void zipFile(ZipOutputStream zipOut, File file, String faterName) throws IOException {
		InputStream input = null;

		byte[] buf = new byte[size];
		input = new FileInputStream(file);
		if (faterName != null) {
			zipOut.putNextEntry(new ZipEntry(faterName + File.separator + file.getName()));
		} else {
			zipOut.putNextEntry(new ZipEntry(File.separator + file.getName()));
		}
		int temp = 0;
		while ((temp = input.read(buf)) != -1) {
			zipOut.write(buf, 0, temp);
		}
		input.close();
	}

	/**
	 * zip解压缩
	 * 
	 * @param zipfile
	 *            File 需要解压缩的文件 "f:\\qwe_16_V6.0.0_1.zip";
	 * @param descDir
	 *            String 解压后的目标目录 "F:\\asd";
	 */
	public static void unFiles(File file, String outputDirectory) throws IOException {
		File outzipFile = new File(outputDirectory);
		if (!outzipFile.exists()) {
			outzipFile.mkdirs();
		}
		byte[] buf = new byte[size];

		File outFile = null;
		ZipFile zipFile = new ZipFile(file);
		ZipInputStream zipInput = new ZipInputStream(new FileInputStream(file));
		ZipEntry entry = null;
		InputStream input = null;
		OutputStream output = null;
		while ((entry = zipInput.getNextEntry()) != null) {
			outFile = new File(outputDirectory + File.separator + entry.getName());

			if (entry.isDirectory()) {
				outFile.mkdir();
			} else {

				if (!outFile.getParentFile().exists()) {
					outFile.getParentFile().mkdirs();
				}

				outFile.createNewFile();
				input = zipFile.getInputStream(entry);
				output = new FileOutputStream(outFile);
				int temp = 0;
				while ((temp = input.read(buf)) != -1) {
					output.write(buf, 0, temp);
				}
				input.close();
				output.close();
			}
		}
		zipFile.close();
		zipInput.close();
	}

	public static void main(String[] args) {
		String dirPath = "D:\\tempFileOfFTP";
		String zipFilePath = "D:\\input.tar";
		try {
			File file = new File(dirPath);
			File zipFile = new File(zipFilePath);
			if (!zipFile.exists()) {
				zipFile.createNewFile();
			}
			ZipFiles(file.listFiles(), zipFile);
			// unFiles(zipFile,dirPath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
