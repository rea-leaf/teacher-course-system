package com.tcs.util.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 命令工具类
 * @author wangbo
 *
 */
public class CommandUtil {
	
	private CommandUtil(){};
	
	public static String execCommand(String command) throws IOException {
		StringBuilder result = new StringBuilder(200);
		BufferedReader in = null;
		Runtime r = Runtime.getRuntime(); // 将要执行的ping命令,此命令是linux格式的命令
		Process process = r.exec(command);
		if (process == null) {
			return result.toString();
		}
		in = new BufferedReader(new InputStreamReader(process.getInputStream())); // 逐行检查输出,计算类似出现=23ms
		String readLine = null;
		while ((readLine = in.readLine()) != null) {
			result.append(readLine).append(" $ ");
		}
		in.close();
		return result.toString();
	}
	
	public static void main(String [] args) {
		try {
			String result = execCommand("jps -v -m");
			System.out.println(result);
			String [] tempResult = result.split("\\$");
			for (String temp : tempResult) {
				System.out.println(temp.trim().substring(0, temp.trim().indexOf(" ")));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
