package com.tcs.util.system;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.StringUtils;
/**
 * 系统
* @Title: MySystemUtil.java
* @Package com.tcs.util.MySystemUtil
* @author 神经刀
* @date 2018年7月29日
* @version V1.0
 */
public class MySystemUtil {

	private MySystemUtil() {};
	
	public static String getProperty(String key) throws NullPointerException {
		String result = "";
		if (StringUtils.isNotBlank(key)) {
			result = System.getProperty(key);
		} else {
			throw new NullArgumentException("无法获取到系统值");
		}
		return result;
	}
}
