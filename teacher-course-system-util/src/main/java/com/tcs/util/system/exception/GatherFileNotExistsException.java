package com.tcs.util.system.exception;

/**
 * 采集配置文件异常
 * @author wangbo
 */
public class GatherFileNotExistsException extends Exception {

	private static final long serialVersionUID = -2088834286149216581L;
	
	public GatherFileNotExistsException(){};
	
	public GatherFileNotExistsException(String message) {
		super(message);
	}

}
