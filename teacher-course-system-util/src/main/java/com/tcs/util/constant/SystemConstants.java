package com.tcs.util.constant;

/**
 * 常量池
 * 
 * @author wangbo
 */
public class SystemConstants {

	private SystemConstants() {};
	
	public static class ReptileConstants {
		
		/**
		 * 发送邮箱HTML方式
		 */
		public static Integer SENDEMAILHTML = 2;
		
		/**
		 * 发送邮箱TXT方式
		 */
		public static Integer SENDEMAILTXT = 1;
		
		/**
		 * APPKEY
		 */
		public static String APPKEY;
		
		public final static String MOGU = "mogu";
	}
	
	public static class JDBCConstants {
		
		/**
		 * 驱动类
		 */
		public static String driver = "org.apache.hive.jdbc.HiveDriver";
		
		/**
		 * 链接
		 */
		public static String url = "jdbc:hive2://slave1:10000/hive_demo1";
		
		/**
		 * 用户名
		 */
		public static String user = "wangbo";
		
		/**
		 * 密码
		 */
		public static String password = "";
	}
	
	/**
	 * 项目标识
	* @Title: SystemConstants.java
	* @Package com.tcs.util.constant
	* @author 神经刀
	* @date 2018年4月12日
	* @version V1.0
	 */
	public static class ProductConstants {
		
		private ProductConstants() {};
		
		/**
		 * 监控
		 */
		public static final String MONITORING = "monitoring";
		
		/**
		 * 股票爬虫
		 */
		public static final String STOCK_CRAWLER = "stock crawler";
	}

	/**
	 * 采集常量池
	 * 
	 * @author wangbo
	 */
	public static class GatherConstants {

		private GatherConstants() {
			super();
		};

		public static final String LOGGER = "logger";

		public static final String AKFKA = "kafka";

		public static final String CONFIGFILE = "config_file";
		
		/**
		 * 正则表达式
		 */
		public static final String REGULAR = "input.regular";

		/**
		 * 输入端的路径
		 */
		public static final String INPUTPATH = "input.path";

		/**
		 * 命令
		 */
		public static final String INPUTCOMMOD = "input.commod";

		/**
		 * 输入端的文件
		 */
		public static final String INPUTFILE = "input.file";

		/**
		 * 输入端的类型
		 */
		public static final String INPUTTYPE = "input.type";

		/**
		 * 输出端的类型
		 */
		public static final String OUTPUTTYPE = "output.type";

		/**
		 * 输出端的集群描述
		 */
		public static final String OUTPUT_BOOTSTRAP_SERVERS = "bootstrap.servers";

		/**
		 * 确认
		 */
		public static final String OUTPUT_ACKS = "acks";

		/**
		 * 重试次数
		 */
		public static final String OUTPUT_RETRIES = "retries";

		/**
		 * 当多个消息要发送到相同分区的时，生产者尝试将消息批量打包在一起，以减少请求交互。这样有助于客户端和服务端的性能提升。该配置的默认批次大小（以字节为单位）：
		 * 不会打包大于此配置大小的消息。 发送到broker的请求将包含多个批次，每个分区一个，用于发送数据。
		 * 较小的批次大小有可能降低吞吐量（批次大小为0则完全禁用批处理）。一个非常大的批次大小可能更浪费内存。因为我们会预先分配这个资源。
		 */
		public static final String OUTPUT_BATCHSIZE = "batch.size";

		/**
		 * 生产者组将发送的消息组合成单个批量请求。正常情况下，只有消息到达的速度比发送速度快的情况下才会出现。但是，在某些情况下，即使在适度的负载下，客户端也可能希望减少请求数量。此设置通过添加少量人为延迟来实现。-
		 * 也就是说，不是立即发出一个消息，生产者将等待一个给定的延迟，以便和其他的消息可以组合成一个批次。这类似于Nagle在TCP中的算法。此设置给出批量延迟的上限：一旦我们达到分区的batch.size值的记录，将立即发送，不管这个设置如何，但是，如果比这个小，我们将在指定的“linger”时间内等待更多的消息加入。此设置默认为0（即无延迟）。假设，设置
		 * linger.ms=5，将达到减少发送的请求数量的效果，但对于在没有负载情况，将增加5ms的延迟。
		 */
		public static final String OUTPUT_LINGERMS = "linger.ms";

		/**
		 * 生产者用来缓存等待发送到服务器的消息的内存总字节数。如果消息发送比可传递到服务器的快，生产者将阻塞max.block.ms之后，抛出异常。
		 * 此设置应该大致的对应生产者将要使用的总内存，但不是硬约束，因为生产者所使用的所有内存都用于缓冲。一些额外的内存将用于压缩（如果启动压缩），以及用于保持发送中的请求。
		 */
		public static final String OUTPUT_BUFFERMEMORY = "buffer.memory";

		/**
		 * KEY序列化
		 */
		public static final String OUTPUT_KEYSERIALIZER = "key.serializer";

		/**
		 * VALUE序列化
		 */
		public static final String OUTPUT_VALUESERIALIZER = "value.serializer";

		/**
		 * 如果enable.auto.commit设置为true，则消费者偏移量自动提交给Kafka的频率（以毫秒为单位）。
		 */
		public static final String INPUT_enableAutoCommit = "enable.auto.commit";

		/**
		 * 此消费者所属消费者组的唯一标识。如果消费者用于订阅或offset管理策略的组管理功能，则此属性是必须的。
		 */
		public static final String INPUT_GROUPID = "group.id";

		/**
		 * 如果enable.auto.commit设置为true，则消费者偏移量自动提交给Kafka的频率（以毫秒为单位）。
		 */
		public static final String INPUT_AUTOCOMMITINTERVALMS = "auto.commit.interval.ms";

		/**
		 * 用于发现消费者故障的超时时间。消费者周期性的发送心跳到broker，表示其还活着。如果会话超时期满之前没有收到心跳，那么broker将从分组中移除消费者，并启动重新平衡。请注意，该值必须在broker配置的group.min.session.timeout.ms和group.max.session.timeout.ms允许的范围内。
		 */
		public static final String INPUT_SESSIONTIMEOUTMS = "session.timeout.ms";
		
		/**
		 * TOPIC
		 */
		public static final String KAFKA_TOPIC = "topic";
		
		public static final String KAFKA_PARTITION = "partition";
		
		/**
		 * KAFKA 消费
		 */
		public static final String [] ConsumerArray = {
				OUTPUT_BOOTSTRAP_SERVERS,
				OUTPUT_ACKS,
				OUTPUT_RETRIES,
				OUTPUT_BATCHSIZE,
				OUTPUT_LINGERMS,
				OUTPUT_BUFFERMEMORY,
				OUTPUT_KEYSERIALIZER,
				OUTPUT_VALUESERIALIZER
				};
		
		/**
		 * KAFKA 生产
		 */
		public static final String [] producerArray = {
				OUTPUT_BOOTSTRAP_SERVERS,
				INPUT_GROUPID,
				INPUT_enableAutoCommit,
				INPUT_AUTOCOMMITINTERVALMS,
				INPUT_SESSIONTIMEOUTMS,
				OUTPUT_KEYSERIALIZER,
				OUTPUT_VALUESERIALIZER
				};
	}
}