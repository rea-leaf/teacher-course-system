package com.tcs.logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;

/**
 * Hello world!
 *
 */
public class InitLoggerConfig {

	private final Logger logger = LoggerFactory.getLogger(InitLoggerConfig.class);

	private String loggerPath;

	public String getLoggerPath() {
		return loggerPath;
	}

	public void setLoggerPath(String loggerPath) {
		this.loggerPath = loggerPath;
	}

	public void init() {
		ConfigurationSource source;
		try {
			File file = ResourceUtils.getFile(loggerPath);
			source = new ConfigurationSource(new FileInputStream(file));  
			Configurator.initialize(null, source);
			logger.info(" 初始化 成功! ");
		} catch (FileNotFoundException e) {
			logger.error("初始化失败 : ", e);
		} catch (IOException e) {
			logger.error("初始化失败 : ", e);
		}
	}
}