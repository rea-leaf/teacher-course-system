package com.tcs.dao.dist;

import org.springframework.stereotype.Repository;

@Repository(value="iDistDao")
public interface IDistDao {

	String getValueByKey(String key);
}
