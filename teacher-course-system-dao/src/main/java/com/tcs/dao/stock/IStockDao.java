package com.tcs.dao.stock;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.tcs.model.base.BaseBean;
import com.tcs.model.stock.StockBean;

/**
 * 股票操作
* @Title: StockServiceImpl.java
* @Package com.tcs.server.stock.impl
* @author 神经刀
* @date 2018年4月9日
* @version V1.0
 */
@Repository(value = "iStockDao")
public interface IStockDao {
	
	/**
	* @Title: getUrl  
	* @Description: 获取链接 
	* @return String    返回类型  
	 */
	String getUrl();
	
	/**
	* @Title: addStockModel  
	* @Description: 新增股票信息 
	* @param StockBean
	* @return int    返回类型  
	 */
	int addStockModel(StockBean StockBean);

	/**
	* @Title: maxId  
	* @Description: 获取最大ID
	* @param @return    参数  
	* @return int    返回类型  
	 */
	int maxId();
	
	/**
	* @Title: queryStockList  
	* @Description: 查询列表
	* @param @return    参数  
	* @return Map<String,Object>    返回类型  
	 */
	List<Map<String,Object>> queryStockList();
}
