package com.tcs.dao.upload;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.tcs.model.upload.UploadBean;

@Repository(value="iUploadDao")
public interface IUploadDao {

	/**
	 * 保存List
	 * @param uploadList
	 * @return
	 */
	public int saveUploadList(List<UploadBean> uploadList);
}