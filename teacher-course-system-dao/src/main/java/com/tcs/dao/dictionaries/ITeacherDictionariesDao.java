package com.tcs.dao.dictionaries;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.tcs.dao.base.IBaseDao;
import com.tcs.model.dictionaries.TeacherDictionariesBean;

/**
 * 字典
 * @author wangBo
 */
@Repository(value="iteacherDictionariesDao")
public interface ITeacherDictionariesDao extends IBaseDao<TeacherDictionariesBean, TeacherDictionariesBean>{
	
	/**
	 * 查询字典数据
	 * @param teacherDictionariesBean
	 * @return
	 */
	public List<TeacherDictionariesBean> queryTeacherDictionariesList(TeacherDictionariesBean teacherDictionariesBean);
	
	/**
	 * 保存字典数据
	 * @param teacherDictionariesBean
	 * @return
	 */
	public int saveTeacherDictionaries(TeacherDictionariesBean teacherDictionariesBean);
}
