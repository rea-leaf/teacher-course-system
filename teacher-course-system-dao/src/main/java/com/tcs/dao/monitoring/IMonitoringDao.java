package com.tcs.dao.monitoring;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.tcs.dao.base.IBaseDao;
import com.tcs.model.monitoring.info.MonitoringBean;
import com.tcs.model.monitoring.info.MonitoringDetalBean;

/**
 * 监控 DAO 接口
 * @author wangbo
 *
 * @param <T>
 * @param <P>
 */
@Repository(value = "iMonitoringDao")
public interface IMonitoringDao extends IBaseDao<MonitoringBean, MonitoringBean> {
	
	/**
	 * 入库多条数据
	 * @param parmas {@link List}参数
	 * @return {@link Boolean}
	 */
	boolean insertMonitoringDetalList(List<MonitoringDetalBean> parmas);
}