package com.tcs.dao.base;

import java.util.List;

/**
 * 顶层DAO层设计
 * 
 * @author wangbo
 * @param <T>
 *            返回值
 * @param <P>
 *            参数
 */
public interface IBaseDao<T, P> {

	/**
	 * 根据参数查询多条数据
	 * 
	 * @param params
	 *            参数
	 * @return {@link List}
	 */
	List<T> queyListByParams(P params);

	/**
	 * 根据参数查询单个数据
	 * 
	 * @param params
	 *            参数
	 * @return T 对象
	 */
	T getOneByParams(P params);

	/**
	 * 入库一条数据
	 * 
	 * @param params
	 *            参数
	 * @return {@link Boolean}
	 */
	boolean insertOne(P params);

	/**
	 * 入库多条数据
	 * 
	 * @param parmas
	 *            {@link List}参数
	 * @return {@link Boolean}
	 */
	boolean insertList(List<P> parmas);

	/**
	 * 删除一条数据
	 * 
	 * @param params
	 *            参数
	 * @return {@link Boolean}
	 */
	boolean deleteByParams(P params);

	/**
	 * 更新数据
	 * 
	 * @param params
	 *            参数
	 * @return {@link Boolean}
	 */
	boolean updateByParams(P params);

}
