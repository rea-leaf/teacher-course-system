package com.tcs.dao.goods;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.tcs.model.goods.GoodsBean;
import com.tcs.model.goods.GoodsParamsBean;
import com.tcs.model.goods.GoodsResultBean;
import com.tcs.model.goods.RegionBean;
import com.tcs.model.goods.RegionGoodsBean;

@Repository(value="iGoodsDao")
public interface IGoodsDao {
	
	/**
	 * 新增区域
	 * @param regionList
	 * @return {@link Integer}
	 */
	int addRegionList(List<RegionBean> regionList);
	
	/**
	 * 新增货物集合
	 * @param goodsBean
	 * @return {@link Integer}
	 */
	int addGoodsList(List<GoodsBean> goodsBean);
	
	/**
	 * 新增货物区域数据
	 * @param regionGoodsBeansList {@link List}
	 * @return {@link Integer}
	 */
	int addGoodsRegionList(List<RegionGoodsBean> regionGoodsBeansList);
	
	/**
	 * 根据参数查询数据
	 * @param goodsParamsBean
	 * @return {@link List}
	 */
	List<GoodsResultBean> queryRegionGoodList(GoodsParamsBean goodsParamsBean);

	/**
	 * 添加商品根据ID
	 * @param goodsParamsBean
	 * @return {@link Integer}
	 */
	int addGoodsById(GoodsParamsBean goodsParamsBean);
	
	/**
	 * 删除商品根据ID
	 * @param goodsParamsBean
	 * @return {@link Integer}
	 */
	int removeGoodsById(GoodsParamsBean goodsParamsBean);
	
	/**
	 * 根据参数查询数据
	 * @param goodsParamsBean
	 * @return {@link List}
	 */
	List<GoodsResultBean> queryCaiGouGoodsListByDate(GoodsParamsBean goodsParamsBean);
	
	/**
	 * 根据ID更改商品状态
	 * @param goodsParamsBean
	 * @return {@link Integer}
	 */
	int updateGoodsStatusById(GoodsParamsBean goodsParamsBean);
	
	/**
	 * 是否存在,根据货物名称
	 * @param goodsParamsBean
	 * @return {@link Integer}
	 */
	int getGoodsByGoodsName(GoodsParamsBean goodsParamsBean);
}