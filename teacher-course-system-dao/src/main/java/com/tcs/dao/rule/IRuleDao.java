package com.tcs.dao.rule;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.tcs.model.rule.RuleBean;
import com.tcs.model.rule.email.EmailRuleBean;
import com.tcs.model.rule.email.user.EmailUserBean;

/**
 * 权限
* @Title: IRuleDao.java
* @Package com.tcs.dao.rule
* @author 神经刀
* @date 2018年4月12日
* @version V1.0
 */
@Repository(value="iRuleDao")
public interface IRuleDao {
	
	/**
	* @Title: addRule  
	* @Description: 新增权限角色信息
	* @param @param ruleBean
	* @param @return    参数  
	* @return int    返回类型  
	 */
	Integer addRule(List<RuleBean> ruleBean);
	
	/**
	 * 
	* @Title: addEmailRule  
	* @Description: 新增邮箱权限数据
	* @param @param emailRuleBean
	* @param @return    参数  
	* @return int    返回类型  
	 */
	Integer addEmailRule(List<EmailRuleBean> emailRuleBean);
	
	/**
	* @Title: addEmailUser  
	* @Description: 新增邮箱数据
	* @param @param emailUserBean
	* @param @return    参数  
	* @return int    返回类型  
	 */
	Integer addEmailUser(List<EmailUserBean> emailUserBean);
	
	/**
	* @Title: getEmailUserExtsis  
	* @Description: 表中是否存在此用户  
	* @param @param emailUserBean
	* @param @return    参数  
	* @return Integer    返回类型  
	 */
	Integer getEmailUserExtsis(EmailUserBean emailUserBean);
	
	/**
	* @Title: queryEmailUserListByRuleName
	* @Description: 根据权限名称查询邮箱用户
	* @param @param ruleBean
	* @param @return    参数  
	* @return List<EmailUserBean>    返回类型  
	 */
	List<EmailUserBean> queryEmailUserListByRuleName(RuleBean ruleBean);
}
