package com.tsc.jmh.model;

import org.openjdk.jmh.annotations.Benchmark;

public class HelloModel {
	
	@Benchmark
    public void wellHelloThere() {
		String str = "";
        for (int i = 0;i < 100; i++) {
        	str += i;
        }
    }
}
