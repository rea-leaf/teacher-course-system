package com.tsc.jmh.test;

import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import com.tsc.jmh.model.HelloModel;

public class JmhTest {

	public static void main(String [] args) {
		try {
			Options opt = new OptionsBuilder()
				    .include(HelloModel.class.getSimpleName())
				    .forks(1)
				    .build();
				new Runner(opt).run();
		} catch (RunnerException e) {
			e.printStackTrace();
		}
	}
}
