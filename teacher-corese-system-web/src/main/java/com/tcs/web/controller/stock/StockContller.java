package com.tcs.web.controller.stock;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tcs.model.rule.email.user.EmailUserBean;
import com.tcs.model.stock.StockResultBean;
import com.tcs.server.rule.IRuleService;
import com.tcs.server.stock.IStockService;

/**
 * 金融控制器
 * 
 * @author wangbo
 */
@RestController
@RequestMapping(value = "stockContller")
public class StockContller {

	private final Logger logger = LoggerFactory.getLogger(StockContller.class);

//	@Autowired
//	private HttpServletRequest request;

	@Resource(name = "iStockService")
	private IStockService iStockService;

	@Resource(name = "iRuleService")
	private IRuleService iRuleService;

	@RequestMapping(value = "/addEmailData", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Integer> addEmailData(EmailUserBean emailUserBean) {
		logger.debug(" emailUserBean : {} " , emailUserBean);
		Map<String, Integer> resultMap = Maps.newHashMap();
		int result = 0;
		if (null != emailUserBean) {
			result = iRuleService.getEmailUserExtsis(emailUserBean);
			if (0 == result) {
				result = iRuleService.addEmailUser(Lists.newArrayList(emailUserBean));
			}
			resultMap.put("result", result);
		} else {
			resultMap.put("result", -1);
		}
		logger.debug(" resultMap : {} " , resultMap);
		return resultMap;
	}
	
	@RequestMapping(value = "/queryStockBeanList", method = RequestMethod.POST)
	@ResponseBody
	public List<StockResultBean> queryStockBeanList() {
		List<StockResultBean> resultList = iStockService.queryStockBeanList();
		logger.debug("size : {} , resultList : {} " , resultList.size() , resultList);
		return resultList;
	}

//	public String getIp() {
//		String ip = request.getHeader("X-Forwarded-For");
//		if (StringUtils.isNotEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)) {
//			// 多次反向代理后会有多个ip值，第一个ip才是真实ip
//			int index = ip.indexOf(",");
//			if (index != -1) {
//				return ip.substring(0, index);
//			} else {
//				return ip;
//			}
//		}
//		ip = request.getHeader("X-Real-IP");
//		if (StringUtils.isNotEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)) {
//			return ip;
//		}
//		return request.getRemoteAddr();
//	}
}