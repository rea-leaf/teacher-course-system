package com.tcs.web.controller.goods;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.tcs.model.goods.GoodsParamsBean;
import com.tcs.model.goods.GoodsResultBean;
import com.tcs.server.goods.IGoodsServer;

@RestController
@RequestMapping(value="goodsController")
public class GoodsController {
	
	private final Logger logger = LoggerFactory.getLogger(GoodsController.class);
	
	@Resource(name = "iGoodsServer")
	private IGoodsServer iGoodsServer;
	
	@RequestMapping(value="/queryRegionGoodList",method=RequestMethod.POST)
	@ResponseBody
	public List<GoodsResultBean> queryRegionGoodList(GoodsParamsBean goodsParamsBean) {
		logger.info(" regionBean : {} " , goodsParamsBean);
		List<GoodsResultBean> goodsResultList = iGoodsServer.queryRegionGoodList(goodsParamsBean);
		HttpServletResponse response = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse();
		response.addHeader("Access-Control-Allow-Origin", "*");
		return goodsResultList;
	}
	
	/**
	 * 添加商品
	 * @param goodsParamsBean
	 * @return
	 */
	@RequestMapping(value="/addGoodsById",method=RequestMethod.POST)
	@ResponseBody
	public Map<String,String> addGoodsById(GoodsParamsBean goodsParamsBean) {
		int result = iGoodsServer.addGoodsById(goodsParamsBean);
		Map<String,String> resultMap = new HashMap<String,String>();
		resultMap.put("result", Integer.valueOf(result).toString());
		HttpServletResponse response = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse();
		response.addHeader("Access-Control-Allow-Origin", "*");
		return resultMap;
	}
	
	/**
	 * 删除商品
	 * @param goodsParamsBean
	 * @return
	 */
	@RequestMapping(value="/removeGoodsbyId",method=RequestMethod.POST)
	@ResponseBody
	public Map<String,String> removeGoodsbyId(GoodsParamsBean goodsParamsBean) {
		int result = iGoodsServer.removeGoodsById(goodsParamsBean);
		Map<String,String> resultMap = new HashMap<String,String>();
		resultMap.put("result", Integer.valueOf(result).toString());
		HttpServletResponse response = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse();
		response.addHeader("Access-Control-Allow-Origin", "*");
		return resultMap;
	}
	
	@RequestMapping(value="/queryCaiGouGoodsListByDate",method=RequestMethod.POST)
	@ResponseBody
	public List<GoodsResultBean> queryCaiGouGoodsListByDate(GoodsParamsBean goodsParamsBean) {
		logger.info("goodsParamsBean : {} " , goodsParamsBean);
		List<GoodsResultBean> resultList = iGoodsServer.queryCaiGouGoodsListByDate(goodsParamsBean);
		HttpServletResponse response = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse();
		response.addHeader("Access-Control-Allow-Origin", "*");
		return resultList;
	}
	
	@RequestMapping(value="/updateGoodsStatusById",method=RequestMethod.POST)
	@ResponseBody
	public GoodsResultBean updateGoodsStatusById(GoodsParamsBean goodsParamsBean) {
		logger.info("goodsParamsBean : {} " , goodsParamsBean);
		GoodsResultBean goodsResultBean = new GoodsResultBean();
		int result = iGoodsServer.updateGoodsStatusById(goodsParamsBean);
		goodsResultBean.setStatus(result);
		HttpServletResponse response = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse();
		response.addHeader("Access-Control-Allow-Origin", "*");
		return goodsResultBean;
	}
	
	@RequestMapping(value="/addGoodsByParams",method=RequestMethod.POST)
	@ResponseBody
	public GoodsResultBean addGoodsByParams(GoodsParamsBean goodsParamsBean) {
		logger.info("goodsParamsBean : {} " , goodsParamsBean);
		GoodsResultBean goodsResultBean = iGoodsServer.addGoodsByParams(goodsParamsBean);
		HttpServletResponse response = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse();
		response.addHeader("Access-Control-Allow-Origin", "*");
		return goodsResultBean;
	}
}