package com.tcs.server.upload;

import java.util.List;

import com.tcs.model.upload.UploadBean;

public interface IUploadServer {

	public int saveUploadList(List<UploadBean> list);
}
