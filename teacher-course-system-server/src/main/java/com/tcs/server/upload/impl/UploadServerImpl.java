package com.tcs.server.upload.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.tcs.dao.upload.IUploadDao;
import com.tcs.model.upload.UploadBean;
import com.tcs.server.upload.IUploadServer;

@Service(value="iUploadServer")
public class UploadServerImpl implements IUploadServer {
	
	private Logger logger = LoggerFactory.getLogger(UploadServerImpl.class);

	@Resource(name = "iUploadDao")
	private IUploadDao iUploadDao;
	
	@Override
	public int saveUploadList(List<UploadBean> list) {
		logger.debug(" list : {} " , list);
		int result = 0;
		if (null != list && !list.isEmpty()) {
			result = iUploadDao.saveUploadList(list);
		}
		return result;
	}

}
