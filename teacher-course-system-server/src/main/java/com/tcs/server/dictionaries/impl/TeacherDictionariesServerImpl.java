package com.tcs.server.dictionaries.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.tcs.dao.dictionaries.ITeacherDictionariesDao;
import com.tcs.model.dictionaries.TeacherDictionariesBean;
import com.tcs.server.dictionaries.ITeacherDictionariesServer;

@Service(value="iTeacherDictionariesServer")
public class TeacherDictionariesServerImpl implements ITeacherDictionariesServer {
	
	private Logger logger = LoggerFactory.getLogger(TeacherDictionariesServerImpl.class);
	
	@Resource(name = "iteacherDictionariesDao")
	private ITeacherDictionariesDao iTeacherDictionariesDao;

	@Override
	public List<TeacherDictionariesBean> queryTeacherDictionariesList(TeacherDictionariesBean teacherDictionariesBean) {
		List<TeacherDictionariesBean> resultList = new ArrayList<TeacherDictionariesBean>();
		if (null != teacherDictionariesBean) {
			resultList = iTeacherDictionariesDao.queryTeacherDictionariesList(teacherDictionariesBean);
		}
		return resultList;
	}

	@Override
	public int saveTeacherDictionaries(TeacherDictionariesBean teacherDictionariesBean) {
		int result = 0;
		if (null != teacherDictionariesBean) {
			result = iTeacherDictionariesDao.saveTeacherDictionaries(teacherDictionariesBean);
		}
		return result;
	}

	@Override
	public TeacherDictionariesBean getTeacherDictionariesBean(TeacherDictionariesBean teacherDictionariesBean) {
		logger.info("teacherDictionariesBean : {} " , teacherDictionariesBean);
		TeacherDictionariesBean resultBean = null;
		List<TeacherDictionariesBean> resultList = new ArrayList<TeacherDictionariesBean>();
		try {
			if (null != teacherDictionariesBean) {
				resultList = iTeacherDictionariesDao.queryTeacherDictionariesList(teacherDictionariesBean);
				if (null != resultList && !resultList.isEmpty()) {
					resultBean = resultList.get(0);
				}
			}
		} catch (Exception e) {
			logger.error("" , e);
		}
 		return resultBean;
	}
}