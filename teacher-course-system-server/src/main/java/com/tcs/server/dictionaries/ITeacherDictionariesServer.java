package com.tcs.server.dictionaries;

import java.util.List;

import com.tcs.model.dictionaries.TeacherDictionariesBean;

/**
 * 字典
 * @author wangBo
 *
 */
public interface ITeacherDictionariesServer {

	/**
	 * 查询字典数据
	 * @param teacherDictionariesBean
	 * @return
	 */
	public List<TeacherDictionariesBean> queryTeacherDictionariesList(TeacherDictionariesBean teacherDictionariesBean);
	
	/**
	 * 保存字典数据
	 * @param teacherDictionariesBean
	 * @return
	 */
	public int saveTeacherDictionaries(TeacherDictionariesBean teacherDictionariesBean);
	
	/**
	 * 查询单个数据
	 * @param teacherDictionariesBean
	 * @return
	 */
	public TeacherDictionariesBean getTeacherDictionariesBean(TeacherDictionariesBean teacherDictionariesBean);
}
