package com.tcs.server.goods.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.tcs.dao.goods.IGoodsDao;
import com.tcs.model.goods.GoodsBean;
import com.tcs.model.goods.GoodsParamsBean;
import com.tcs.model.goods.GoodsResultBean;
import com.tcs.model.goods.RegionBean;
import com.tcs.model.goods.RegionGoodsBean;
import com.tcs.server.goods.IGoodsServer;
import com.tcs.util.date.DateUtil;
import com.tcs.util.uuid.UUIDUtil;

/**
 * 货物接口实现类
 * @author wangbo
 */
@Service(value="iGoodsServer")
public class GoodsServerImpl implements IGoodsServer {
	
	private final Logger logger = LoggerFactory.getLogger(GoodsServerImpl.class);
	
	@Resource(name="iGoodsDao")
	private IGoodsDao iGoodsDao;

	@Override
	public int addRegionList(List<RegionBean> regionList) {
		int result = 0;
		if (null != regionList && !regionList.isEmpty()) {
			result = iGoodsDao.addRegionList(regionList);
		}
		return result;
	}

	@Override
	public int addGoodsList(List<GoodsBean> goodsBean) {
		int result = 0;
		if (null != goodsBean && !goodsBean.isEmpty()) {
			result = iGoodsDao.addGoodsList(goodsBean);
		}
		return result;
	}

	@Override
	public int addGoodsRegionList(List<RegionGoodsBean> regionGoodsBeansList) {
		int result = 0;
		if (null != regionGoodsBeansList && !regionGoodsBeansList.isEmpty()) {
			result = iGoodsDao.addGoodsRegionList(regionGoodsBeansList);
		}
		return result;
	}
	
	@Override
	public List<GoodsResultBean> queryRegionGoodList(GoodsParamsBean goodsParamsBean) {
		List<GoodsResultBean> resultList = new ArrayList<GoodsResultBean>();
		if (null != goodsParamsBean) {
			goodsParamsBean.setBeginTime(DateUtil.getBeginOfToday() / 1000);
			goodsParamsBean.setEndTime(DateUtil.getEndOfToday() / 1000);
			resultList = iGoodsDao.queryRegionGoodList(goodsParamsBean);
		}
		return resultList;
	}
	
	@Override
	public int addGoodsById(GoodsParamsBean goodsParamsBean) {
		int result = 0;
		if (null != goodsParamsBean && null != goodsParamsBean.getGoodsId()) {
			goodsParamsBean.setId(UUIDUtil.getUUID());
			result = iGoodsDao.addGoodsById(goodsParamsBean);
		}
		return result;
	}
	
	@Override
	public int removeGoodsById(GoodsParamsBean goodsParamsBean) {
		int result = 0;
		if (null != goodsParamsBean && null != goodsParamsBean.getGoodsId()) {
			goodsParamsBean.setId(UUIDUtil.getUUID());
			goodsParamsBean.setBeginTime(DateUtil.getBeginOfToday() / 1000);
			goodsParamsBean.setEndTime(DateUtil.getEndOfToday() / 1000);
			result = iGoodsDao.removeGoodsById(goodsParamsBean);
		}
		return result;
	}
	
	/**
	 * 查询采购商品根据时间
	 */
	@Override
	public List<GoodsResultBean> queryCaiGouGoodsListByDate(GoodsParamsBean goodsParamsBean) {
		goodsParamsBean.setBeginTime(DateUtil.getBeginByStr(goodsParamsBean.getDate()) / 1000);
		goodsParamsBean.setEndTime(DateUtil.getEndByStr(goodsParamsBean.getDate()) / 1000);
		List<GoodsResultBean> result = iGoodsDao.queryCaiGouGoodsListByDate(goodsParamsBean);
		return result;
	}
	
	/**
	 * 更新商品状态根据ID
	 */
	@Override
	public int updateGoodsStatusById(GoodsParamsBean goodsParamsBean) {
		int result = iGoodsDao.updateGoodsStatusById(goodsParamsBean);
		return result;
	}
	
	/**
	 * 是否存在,根据货物名称
	 */
	@Override
	public int getGoodsByGoodsName(GoodsParamsBean goodsParamsBean) {
		int result = 0;
		if (null != goodsParamsBean) {
			result = iGoodsDao.getGoodsByGoodsName(goodsParamsBean);
		}
		return result;
	}
	
	/**
	 * 新增商品根据参数
	 */
	@Override
	public GoodsResultBean addGoodsByParams(GoodsParamsBean goodsParamsBean) {
		GoodsResultBean resultBean = null;
		if (null != goodsParamsBean && !StringUtils.isEmpty(goodsParamsBean.getGoodsName()) && !StringUtils.isEmpty(goodsParamsBean.getRegionId())) {
			if (this.getGoodsByGoodsName(goodsParamsBean) > 0) {
				resultBean = new GoodsResultBean("此商品[" + goodsParamsBean.getGoodsName() + "]已经处在!");
			} else {
				try {
					GoodsBean goodsBean = new GoodsBean();
					goodsBean.setId(UUIDUtil.getUUID());
					goodsBean.setGoodsName(goodsParamsBean.getGoodsName());
					List<GoodsBean> goodsList = new ArrayList<GoodsBean>();
					goodsList.add(goodsBean);
					this.addGoodsList(goodsList);
					
					RegionGoodsBean regionGoodsBean = new RegionGoodsBean(goodsBean.getId(),goodsParamsBean.getRegionId(),0,UUIDUtil.getUUID());
					List<RegionGoodsBean> regionGoodsList = new ArrayList<RegionGoodsBean>();
					regionGoodsList.add(regionGoodsBean);
					this.addGoodsRegionList(regionGoodsList);
					resultBean = new GoodsResultBean("此商品[" + goodsParamsBean.getGoodsName() + "]保存成功!");
				} catch (Exception e) {
					resultBean = new GoodsResultBean("发生异常!");
					logger.error("" , e);
				}
			}
		} else {
			resultBean = new GoodsResultBean("此商品[" + goodsParamsBean.getGoodsName() + "]参数有空!");
		}
		return resultBean;
	}
}