package com.tcs.server.goods;

import java.util.List;

import com.tcs.model.goods.GoodsBean;
import com.tcs.model.goods.GoodsParamsBean;
import com.tcs.model.goods.GoodsResultBean;
import com.tcs.model.goods.RegionBean;
import com.tcs.model.goods.RegionGoodsBean;

/**
 * 货物接口
 * @author wangbo
 */
public interface IGoodsServer {

	/**
	 * 新增区域
	 * @param regionList
	 * @return
	 */
	int addRegionList(List<RegionBean> regionList);
	
	/**
	 * 新增货物集合
	 * @param goodsBean
	 * @return
	 */
	int addGoodsList(List<GoodsBean> goodsBean);
	
	/**
	 * 新增货物区域数据
	 * @param regionGoodsBeansList {@link List}
	 * @return {@link Integer}
	 */
	int addGoodsRegionList(List<RegionGoodsBean> regionGoodsBeansList);
	
	/**
	 * 根据参数查询数据
	 * @param goodsParamsBean
	 * @return {@link List}
	 */
	List<GoodsResultBean> queryRegionGoodList(GoodsParamsBean goodsParamsBean);
	
	/**
	 * 添加商品根据ID
	 * @param goodsParamsBean
	 * @return
	 */
	int addGoodsById(GoodsParamsBean goodsParamsBean);
	
	/**
	 * 删除商品根据ID
	 * @param goodsParamsBean
	 * @return
	 */
	int removeGoodsById(GoodsParamsBean goodsParamsBean);
	
	/**
	 * 根据参数查询数据
	 * @param goodsParamsBean
	 * @return {@link List}
	 */
	List<GoodsResultBean> queryCaiGouGoodsListByDate(GoodsParamsBean goodsParamsBean);
	
	/**
	 * 根据ID更改商品状态
	 * @param goodsParamsBean
	 * @return {@link Integer}
	 */
	int updateGoodsStatusById(GoodsParamsBean goodsParamsBean);
	
	/**
	 * 是否存在,根据货物名称
	 * @param goodsParamsBean
	 * @return {@link Integer}
	 */
	int getGoodsByGoodsName(GoodsParamsBean goodsParamsBean);
	
	/**
	 * 新增商品根据条件 
	 * @param goodsParamsBean {@link GoodsParamsBean}
	 * @return {@link GoodsResultBean}
	 */
	GoodsResultBean addGoodsByParams(GoodsParamsBean goodsParamsBean);
}