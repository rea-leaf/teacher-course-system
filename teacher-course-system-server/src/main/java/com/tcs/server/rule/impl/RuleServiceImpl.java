package com.tcs.server.rule.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.tcs.dao.rule.IRuleDao;
import com.tcs.model.rule.RuleBean;
import com.tcs.model.rule.email.EmailRuleBean;
import com.tcs.model.rule.email.user.EmailUserBean;
import com.tcs.server.rule.IRuleService;

/**
 * 权限
* @Title: RuleServiceImpl.java
* @Package com.tcs.server.rule
* @author 神经刀
* @date 2018年4月12日
* @version V1.0
 */
@Service(value="iRuleService")
public class RuleServiceImpl implements IRuleService {
	
	@Resource(name="iRuleDao")
	private IRuleDao iRuleDao;

	@Override
	public Integer addRule(List<RuleBean> ruleBean) {
		int result = 0;
		if (null != ruleBean) {
			result = iRuleDao.addRule(ruleBean);
		}
		return result;
	}

	@Override
	public Integer addEmailRule(List<EmailRuleBean> emailRuleBean) {
		int result = 0;
		if (null != emailRuleBean) {
			result = iRuleDao.addEmailRule(emailRuleBean);
		}
		return result;
	}
	
	@Override
	public Integer addEmailUser(List<EmailUserBean> emailUserBean) {
		int result = 0;
		if (null != emailUserBean) {
			result = iRuleDao.addEmailUser(emailUserBean);
		}
		return result;
	}
	
	@Override
	public List<EmailUserBean> queryEmailUserListByRuleName(RuleBean ruleBean) {
		List<EmailUserBean> ruleUserList = null;
		if (null != ruleBean) {
			ruleUserList = iRuleDao.queryEmailUserListByRuleName(ruleBean);
		}
		return ruleUserList;
	}
	
	@Override
	public String[] getCcs(RuleBean ruleBean) {
		List<EmailUserBean> emailUserList = this.queryEmailUserListByRuleName(ruleBean);
		String [] cCs = new String[emailUserList.size()];
		int index = 0;
		for (EmailUserBean emailUserBean : emailUserList) {
			cCs[index] = emailUserBean.getEmail();
			index++;
		}
		return cCs;
	}
	
	@Override
	public Integer getEmailUserExtsis(EmailUserBean emailUserBean) {
		return iRuleDao.getEmailUserExtsis(emailUserBean);
	}
}
