package com.tcs.server.enums;

import javax.annotation.Resource;

import com.tcs.model.dictionaries.TeacherDictionariesBean;
import com.tcs.server.dictionaries.ITeacherDictionariesServer;
import com.tcs.util.spring.SpringConfigTool;

public enum DictionariesEnum {

	INTFACE;
	
	private TeacherDictionariesBean teacherDictionariesBean;
	
	@Resource(name= "springConfigTool")
	private SpringConfigTool springConfigTool;
	
	public TeacherDictionariesBean getTeacherDictionariesBean() {
		if (null == teacherDictionariesBean) {
			System.out.println("=----------------------------=  初始化");
			ITeacherDictionariesServer dictionariesServer = (ITeacherDictionariesServer) SpringConfigTool.getBean("iTeacherDictionariesServer");
			teacherDictionariesBean = dictionariesServer.getTeacherDictionariesBean(new TeacherDictionariesBean(1));
		} else  {
			System.out.println("=----------------------------=  获取对象成功!");
		}
		return teacherDictionariesBean;
	}
}