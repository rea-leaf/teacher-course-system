package com.tcs.server.monitoring;

import java.util.List;

import com.tcs.model.monitoring.info.MonitoringDetalBean;
import com.tcs.server.base.IBaseServers;

/**
 * 监控数据入库接口
 * @author wangbo
 */
public interface IMonitoringServer<T, P> extends IBaseServers<T, P> {
	
	/**
	 * 入库多条数据
	 * @param parmas {@link List}参数
	 * @return {@link Boolean}
	 */
	boolean insertMonitoringDetalList(List<MonitoringDetalBean> parmas);
}