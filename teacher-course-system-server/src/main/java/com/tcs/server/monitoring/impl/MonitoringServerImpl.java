package com.tcs.server.monitoring.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.tcs.dao.monitoring.IMonitoringDao;
import com.tcs.model.monitoring.info.MonitoringBean;
import com.tcs.model.monitoring.info.MonitoringDetalBean;
import com.tcs.server.monitoring.IMonitoringServer;

/**
 * 监控Server层实现
 * @author wangbo
 */
@Service("monitoringServerImpl")
public class MonitoringServerImpl implements IMonitoringServer<MonitoringBean, MonitoringBean> {
	
	private final Logger logger = LoggerFactory.getLogger(MonitoringServerImpl.class);
	
	@Resource(name = "iMonitoringDao")
	private IMonitoringDao iMonitoringDao;

	@Override
	public List<MonitoringBean> queyListByParams(MonitoringBean params) {
		return null;
	}

	@Override
	public MonitoringBean getOneByParams(MonitoringBean params) {
		return null;
	}

	@Override
	public boolean insertOne(MonitoringBean params) {
		boolean result = false;
		if (null != params) {
			try {
				result = iMonitoringDao.insertOne(params);
				this.insertMonitoringDetalList(params.getDetalBeanList());
				logger.debug("result : {} , params : {}  " , result , params );
			} catch (Exception e) {
				logger.error("" , e);
			}
		}
		return result;
	}
	
	@Override
	public boolean insertMonitoringDetalList(List<MonitoringDetalBean> parmas) {
		boolean result = false;
		if (null != parmas) {
			try {
				logger.debug("result : {} , params : {}  " , result , parmas );
				result = iMonitoringDao.insertMonitoringDetalList(parmas);
				logger.debug("result : {} , params : {}  " , result , parmas );
			} catch (Exception e) {
				logger.error("" , e);
			}
		}
		return result;
	}

	@Override
	public boolean insertList(List<MonitoringBean> parmas) {
		boolean result = false;
		if (null != parmas && !parmas.isEmpty()) {
			try {
				result = iMonitoringDao.insertList(parmas);
				logger.debug("result : {} " , result);
			} catch (Exception e) {
				logger.error("" , e);
			}
		}
		return result;
	}

	@Override
	public boolean deleteByParams(MonitoringBean params) {
		return false;
	}

	@Override
	public boolean updateByParams(MonitoringBean params) {
		return false;
	}

	public IMonitoringDao getiMonitoringDao() {
		return iMonitoringDao;
	}

	public void setiMonitoringDao(IMonitoringDao iMonitoringDao) {
		this.iMonitoringDao = iMonitoringDao;
	}

}