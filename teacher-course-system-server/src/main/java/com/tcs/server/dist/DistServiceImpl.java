package com.tcs.server.dist;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.tcs.dao.dist.IDistDao;

/**
* @Title: DistServiceImpl.java
* @Package com.tcs.server.dist
* @author 神经刀
* @date 2018年7月29日
* @version V1.0
 */
@Service("distServiceImpl")
public class DistServiceImpl implements IDistService {
	
	@Resource(name="iDistDao")
	private IDistDao iDistDao;

	@Override
	public String getValueByKey(String key) {
		return iDistDao.getValueByKey(key);
	}
}