package com.tcs.server.dist;

/**
 * 字典
* @Title: DistService.java
* @Package com.tcs.server.dist
* @author 神经刀
* @date 2018年7月29日
* @version V1.0
 */
public interface IDistService {
	
	String getValueByKey(String key);

}