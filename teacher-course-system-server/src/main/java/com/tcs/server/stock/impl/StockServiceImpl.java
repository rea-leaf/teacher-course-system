package com.tcs.server.stock.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.google.common.collect.Lists;
import com.tcs.dao.stock.IStockDao;
import com.tcs.model.stock.StockBean;
import com.tcs.model.stock.StockCheckBean;
import com.tcs.model.stock.StockResultBean;
import com.tcs.server.stock.IStockService;

/**
 * 股票操作
* @Title: StockServiceImpl.java
* @Package com.tcs.server.stock.impl
* @author 神经刀
* @date 2018年4月9日
* @version V1.0
 */
@Service(value="iStockService")
public class StockServiceImpl implements IStockService {
	
	@Resource(name="iStockDao")
	private IStockDao istockDao;

	@Override
	public String getUrl() {
		String url = istockDao.getUrl();
		if (StringUtils.isNotBlank(url)) {
			url = url.trim();
		}
		return url;
	}

	@Override
	public Integer addStockModel(StockBean StockBean) {
		return istockDao.addStockModel(StockBean);
	}

	@Override
	public Integer maxId() {
		return istockDao.maxId();
	}
	
	@Override
	public List<Map<String, Object>> queryStockList() {
		PageHelper.startPage(1,3);
		return istockDao.queryStockList();
	}
	
	@Override
	public List<StockResultBean> queryStockBeanList() {
		List<Map<String,Object>> tempResult = this.queryStockList();
		List<StockResultBean> resultList = Lists.newArrayList();
		StockResultBean stockResultBean = null;
		StockCheckBean stockCheckBean = null;
		StockCheckBean monitoringCheckBean = null;
		for (Map<String,Object> temp : tempResult) {
			stockCheckBean = new StockCheckBean(NumberUtils.toInt(temp.get("stockStatus").toString()), temp.get("email_id").toString() , temp.get("stock").toString());
			monitoringCheckBean = new StockCheckBean(NumberUtils.toInt(temp.get("monitoringStatus").toString()), temp.get("email_id").toString() , temp.get("monitoring").toString());
			stockResultBean = new StockResultBean(temp.get("user_name").toString(),temp.get("email").toString(),stockCheckBean,monitoringCheckBean);
			resultList.add(stockResultBean);
		}
		return resultList;
	}
}