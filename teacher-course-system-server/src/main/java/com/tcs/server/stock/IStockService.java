package com.tcs.server.stock;

import java.util.List;
import java.util.Map;

import com.tcs.model.stock.StockBean;
import com.tcs.model.stock.StockResultBean;

public interface IStockService {

	/**
	* @Title: getUrl  
	* @Description: 获取链接 
	* @return String    返回类型  
	 */
	String getUrl();
	
	/**
	* @Title: addStockModel  
	* @Description: 新增股票信息 
	* @param StockBean
	* @return {@link Integer}
	 */
	Integer addStockModel(StockBean StockBean);
	
	/**
	* @Title: maxId  
	* @Description: 获取最大ID
	* @param @return    参数  
	* @return {@link Integer}
	 */
	Integer maxId();
	
	/**
	* @Title: queryStockList  
	* @Description: 查询列表
	* @param @return    参数  
	* @return List<Map<String,Object>>    返回类型  
	 */
	List<Map<String,Object>> queryStockList();
	
	/**
	* @Title: queryStockBeanList  
	* @Description: 查询结果集
	* @param @return    参数  
	* @return List<StockResultBean>    返回类型  
	 */
	List<StockResultBean> queryStockBeanList();
}