package com.tcs.ftp.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FTP 服务器
 */
public class App {
	
	private final Logger logger = LoggerFactory.getLogger(App.class);
	
	public static void main(String[] args) {
		new App().run();
	}
	
	public void run() {
		logger.debug(" ftp 服务器启动 : {} " , "OK");
		try {
			new Thread(new FtpStart() , "ftp 线程").start();
		} catch (Exception e) {
			logger.error("" , e);
		}
	}
	
	final class FtpStart implements Runnable {

		@Override
		public void run() {
			
		}
		
	}
	
}
