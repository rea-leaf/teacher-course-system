package com.tcs.redis.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * Redis 注解
* @Title: RedisCache.java
* @Package com.tcs.redis.annotations
* @author 神经刀
* @date 2018年6月13日
* @version V1.0
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisCache {

	/**
	* @Title: saveTime
	* @Description: 设置保存时间  
	* @return long    返回类型  
	 */
	long saveTime() default 0;
	
	/**
	 * 时间单位设置
	 * @return {@link TimeUnit}
	 */
	TimeUnit timeUnit() default TimeUnit.MICROSECONDS;
	
}