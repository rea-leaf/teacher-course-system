package com.tcs.reptile.service;

/**
 * 运行接口
* @Title: RunPoac.java
* @Package com.tcs.reptile.service
* @author 神经刀
* @date 2018年7月29日
* @version V1.0
 */
public interface RunPoac {

	/**
	* @Title: SingleRun  
	* @Description: 单行程运行
	* @throws Exception
	 */
	void SingleRun() throws Exception;
	
	/**
	 * 
	* @Title: concurrentRun  
	* @Description: 并发运行  
	* @throws
	 */
	void concurrentRun() throws Exception;
}