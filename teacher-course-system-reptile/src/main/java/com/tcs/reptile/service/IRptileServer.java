package com.tcs.reptile.service;

public interface IRptileServer {

	/**
	* @Title: work  
	* @Description: 干起来!
	* @param     参数  
	* @return void    返回类型  
	* @throws
	 */
	void work();
	
	/**
	* @Title: init  
	* @Description: 初始化
	* @param     参数  
	* @return void    返回类型  
	 */
	void init();
}