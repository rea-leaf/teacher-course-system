package com.tcs.reptile.service;

/**
 * 日期效验,缓存
* @Title: SeekServer.java
* @Package com.tcs.reptile.service
* @author 神经刀
* @date 2018年6月18日
* @version V1.0
 */
public interface SeekServer {

	/**
	* @Title: isSeek  
	* @Description  2节假日,1休息日,0工作日  <code>true</code> : 2节假日,1休息日  , <code>false</code> : 工作日  
	* @return {@link Integer}
	 */
	Integer isSeek();
}
