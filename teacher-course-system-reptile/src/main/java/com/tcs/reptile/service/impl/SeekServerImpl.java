package com.tcs.reptile.service.impl;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.tcs.redis.annotations.RedisCache;
import com.tcs.reptile.service.SeekServer;
import com.tcs.util.http.HttpClientUtil;

import net.sf.json.JSONObject;

/**
* @Title: SeekServerImpl.java
* @Package com.tcs.reptile.service.impl
* @author 神经刀
* @date 2018年6月18日
* @version V1.0
 */
@Service(value="seekServer")
public class SeekServerImpl implements SeekServer {
	
	private final Logger logger = LoggerFactory.getLogger(SeekServerImpl.class);
	
	private DateTime dateTime = new DateTime(new Date());
	
	private String url = "http://api.goseek.cn/Tools/holiday?date=";

	@RedisCache(saveTime=24,timeUnit=TimeUnit.HOURS)
	@Override
	public Integer isSeek() {
		int isHoliday = 0;
		try {
			url = getUrl(url,dateTime.toString("yyyyMMdd"));
			String holiday = HttpClientUtil.get(url);
			logger.info(" url : {} , 结果 : {} " , url , holiday);
			JSONObject jsonObject = JSONObject.fromObject(holiday);
			isHoliday = jsonObject.getInt("data");
		} catch (KeyManagementException e) {
			logger.error("", e);
		} catch (NoSuchAlgorithmException e) {
			logger.error("", e);
		} catch (KeyStoreException e) {
			logger.error("", e);
		} catch (IOException e) {
			logger.error("", e);
		}
		return isHoliday;
	}

	/**
	 * 
	* @Title: getUrl  
	* @Description: 组装URL
	* @param url
	* @param params
	* @return {@link String}
	 */
	private String getUrl(String url,Object params) {
		StringBuffer urlSb = new StringBuffer(url).append(params.toString());
		return urlSb.toString();
	}
}