package com.tcs.reptile.proxyip;

import java.util.List;

import com.tcs.model.IpBean;

/**
 * 代理IP接口
* @Title: ProxyIpService.java
* @Package com.tcs.reptile.proxyip
* @author 神经刀
* @date 2018年7月29日
* @version V1.0
 */
public interface ProxyIpService {

	/**
	* @Title: getMyProxyPoll  
	* @Description: 获取代理池的可用代理  
	* @return {@link IpBean}
	 */
	IpBean getMyProxyPoll();
	
	/**
	* @Title: getMyProxyPollList  
	* @Description: 获取代理池的可用代理  
	* @param size : 个数
	* @return {@link List<IpBean>}
	 */
	List<IpBean> getMyProxyPollList(Integer size);
	
	/**
	* @Title: getChargeVPSProxyIp
	* @Description: 收费的代理IP  
	* @return {@link IpBean}
	 */
	IpBean getChargeVPSProxyIp();
	
	/**
	* @Title: getChargeVPSProxyIpList  
	* @Description: 获取收费VPS代理池的可用代理  
	* @param size : 个数
	* @return {@link List<IpBean>}
	 */
	List<IpBean> getChargeVPSProxyIpList(Integer size);
}