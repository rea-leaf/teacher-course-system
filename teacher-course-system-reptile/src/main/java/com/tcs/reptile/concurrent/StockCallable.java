package com.tcs.reptile.concurrent;

import java.util.concurrent.Callable;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcs.model.email.MailSenderInfo;
import com.tcs.model.stock.StockBean;
import com.tcs.rpc.service.IEmailService;
import com.tcs.server.stock.IStockService;
import com.tcs.util.http.HttpClientUtil;
import com.tcs.util.uuid.UUIDUtil;

/**
 * 并发模型
* @Title: StockCallable.java
* @Package com.tcs.reptile.concurrent
* @author 神经刀
* @date 2018年6月18日
* @version V1.0
 */
public class StockCallable implements Callable<Boolean> {
	
	private final Logger logger = LoggerFactory.getLogger(StockCallable.class);
	
	private String url;
	
	private int maxId;
	
	private IStockService iStockService;
	
	private IEmailService iEmailService;
	
	private MailSenderInfo mailInfo;
	
	private String [] Ccs;
	
	public StockCallable() {};
	
	public StockCallable(String url, int maxId, IStockService iStockService, IEmailService iEmailService) {
		super();
		this.url = url;
		this.maxId = maxId;
		this.iStockService = iStockService;
		this.iEmailService = iEmailService;
	}

	public StockCallable(String url, int maxId, IStockService iStockService, IEmailService iEmailService,
			MailSenderInfo mailInfo, String[] ccs) {
		super();
		this.url = url;
		this.maxId = maxId;
		this.iStockService = iStockService;
		this.iEmailService = iEmailService;
		this.mailInfo = mailInfo;
		this.Ccs = ccs;
	}

	@Override
	public Boolean call() throws Exception {
		boolean result = true;
		StockBean stockBean = null;
		String html,title,content = "";
		html = HttpClientUtil.get(getUrl(url , maxId));
		Document document = Jsoup.parse(html);
		document.select("img").remove();
		title = document.title();
		Elements main = document.select("div.main");
		logger.info(" --------- url : {} \n , title : {} \n, content : {}" , url + maxId , title, main);
		if (StringUtils.isNotBlank(main.html())) {
			content = main.html();
			stockBean = new StockBean(UUIDUtil.getUUID(),title,content,maxId);
			try {
				iStockService.addStockModel(stockBean);
			} catch (Exception e) {
				logger.error(" 第一次入库 录入失败 : \n  stockBean : " + stockBean, e);
				try {
					stockBean.setContent("");
					iStockService.addStockModel(stockBean);
				} catch (Exception e1) {
					logger.error(" 第二次入库 录入失败 : \n  stockBean : " + stockBean, e1);
				}
			}
			setMailSenderInfo(stockBean);
			if (null != mailInfo) {
				logger.info("mailInfo : {} " , mailInfo);
				iEmailService.send(mailInfo);
				logger.info("发送成功! 数据为 : {} " , mailInfo);
			}
		} else {
			result = false;
		}
		return result;
	}
	
	/**
	 * 
	* @Title: getUrl  
	* @Description: 组装URL
	* @param url
	* @param params
	* @return {@link String}
	 */
	private String getUrl(String url,Object params) {
		StringBuffer urlSb = new StringBuffer(url).append(params.toString());
		return urlSb.toString();
	}
	
	private void setMailSenderInfo(StockBean stockBean) {
		mailInfo = new MailSenderInfo();
		mailInfo.setSubject(stockBean.getTitle());
		mailInfo.setSendFlag(2);	// HTML 格式
		mailInfo.setContent(stockBean.getTitle() + "<br>" + stockBean.getContent());
		mailInfo.setCcs(Ccs);
	}
}