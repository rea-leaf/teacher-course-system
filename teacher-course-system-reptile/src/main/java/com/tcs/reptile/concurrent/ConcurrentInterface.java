package com.tcs.reptile.concurrent;

/**
 * 接口
* @Title: ConcurrentInterface.java
* @Package com.tcs.reptile.concurrent
* @author 神经刀
* @date 2018年7月20日
* @version V1.0
 */
public interface ConcurrentInterface {

	/**
	* @Title: job  
	* @Description: JOB  
	* @param @throws Exception    参数  
	* @return void    返回类型  
	 */
	public void job() throws Exception;
	
	/**
	* @Title: over  
	* @Description: over
	* @param     参数  
	* @return void    返回类型  
	 */
	public void over();
}
