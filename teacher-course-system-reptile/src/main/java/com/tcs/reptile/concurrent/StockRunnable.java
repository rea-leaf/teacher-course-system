package com.tcs.reptile.concurrent;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcs.model.email.MailSenderInfo;
import com.tcs.rpc.service.IEmailService;
import com.tcs.server.stock.IStockService;

/**
 * 并发模型
 * 
 * @Title: StockRunnable.java
 * @Package com.tcs.reptile.concurrent
 * @author 神经刀
 * @date 2018年6月27日
 * @version V1.0
 */
public class StockRunnable extends AbstractStockTask implements Runnable {

	private final Logger logger = LoggerFactory.getLogger(StockCallable.class);
	
	public StockRunnable() {
		super();
	}

	public StockRunnable(String url, int maxId, IStockService iStockService, IEmailService iEmailService,
			MailSenderInfo mailInfo, String[] ccs, CountDownLatch countDownLatch, AtomicInteger atomicInteger) {
		super(url, maxId, iStockService, iEmailService, mailInfo, ccs, countDownLatch, atomicInteger);
	}

	public StockRunnable(String url, int maxId, IStockService iStockService, IEmailService iEmailService,
			MailSenderInfo mailInfo, String[] ccs) {
		super(url, maxId, iStockService, iEmailService, mailInfo, ccs);
	}

	public StockRunnable(String url, int maxId, IStockService iStockService, IEmailService iEmailService) {
		super(url, maxId, iStockService, iEmailService);
	}

	@Override
	public void run() {
		try {
			this.work();
		} catch (Exception e) {
			logger.error("" , e);
		}
	}
}