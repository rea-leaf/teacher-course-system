package com.tcs.reptile.model;

import com.tcs.model.stock.StockBean;

/**
 * 并发模型
* @Title: TeentModel.java
* @Package com.tcs.reptile.model
* @author 神经刀
* @date 2018年6月18日
* @version V1.0
 */
public class TeentModel {

	// 采集模型
	private StockBean stockBean;
	
	// 标识
	private Boolean flag;

	public StockBean getStockBean() {
		return stockBean;
	}

	public void setStockBean(StockBean stockBean) {
		this.stockBean = stockBean;
	}

	public Boolean getFlag() {
		return flag;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}
}