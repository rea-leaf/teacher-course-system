package com.tcs.start;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 网络爬虫<br>
 * 定时任务
 * @Title: App.java
 * @Package com.tcs.start
 * @author 神经刀
 * @date 2018年4月9日
 * @version V1.0
 */
public class App {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext app = new ClassPathXmlApplicationContext("classpath:spring/app.xml");
		app.registerShutdownHook();
	}
	
}