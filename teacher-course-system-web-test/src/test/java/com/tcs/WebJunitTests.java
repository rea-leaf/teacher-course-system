package com.tcs;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/spring-servlet.xml" , "classpath:spring/app.xml" })
@WebAppConfiguration
public class WebJunitTests extends AbstractTestNGSpringContextTests {

	private final Logger logger = LoggerFactory.getLogger(WebJunitTests.class);

	protected MockMvc mockMvc;

	@Autowired
	protected WebApplicationContext webApplicationContext;

	@Before
	public void setup() {
		try {
			this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
			logger.info(" mockMvc: {} ", mockMvc);
		} catch (Exception e) {
			logger.error("", e);
		}
	}

	@Test
	public void queryStockBeanList() {
		String result = "";
		try {
			result = mockMvc.perform(MockMvcRequestBuilders.post("/stockContller/queryStockBeanList") // 请求的url,请求的方法是get
					.contentType(MediaType.APPLICATION_FORM_URLENCODED) // 数据的格式
			// 添加参数
			).andExpect(MockMvcResultMatchers.status().isOk()) // 返回的状态是200
					.andDo(MockMvcResultHandlers.print()) // 打印出请求和相应的内容
					.andReturn().getResponse().getContentAsString();
		} catch (Exception e) {
			logger.error("", e);
		}
		logger.info(" result : {} , mockMvc : {}  " , result , mockMvc); // 将相应的数据转换为字符串);
	}
}