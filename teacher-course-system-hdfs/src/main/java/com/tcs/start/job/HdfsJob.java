package com.tcs.start.job;

/**
* @Title: HdfsJob.java
* @Package com.tcs.start.job
* @author 神经刀
* @date 2018年3月28日
* @version V1.0
 */
public interface HdfsJob {
	
	void run();
}