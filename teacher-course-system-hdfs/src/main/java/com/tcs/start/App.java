package com.tcs.start;

import com.tcs.start.job.HdfsJob;
import com.tcs.start.job.impl.HdfsJobImpl;

/**
* @Title: App.java
* @Package com.tcs.start
* @author 神经刀
* @date 2018年3月28日
* @version V1.0
 */
public class App {
	
	public static void main(String[] args) {
		HdfsJob hdfsJob = new HdfsJobImpl();
		hdfsJob.run();
	}
}
