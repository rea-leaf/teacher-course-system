package com.tcs.test.mybatis;

import javax.annotation.Resource;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcs.dao.dictionaries.ITeacherDictionariesDao;
import com.tcs.model.dictionaries.TeacherDictionariesBean;
import com.tcs.test.BaseJunit4Test;
import com.tcs.util.uuid.UUIDUtil;

public class MybatisTest extends BaseJunit4Test {

	private Logger logger = LoggerFactory.getLogger(MybatisTest.class);
	
	@Resource(name = "iteacherDictionariesDao")
	private ITeacherDictionariesDao iTeacherDictionariesDao;
	
	@Test
	public void run() {
		TeacherDictionariesBean bean = new TeacherDictionariesBean(
				UUIDUtil.getUUID(),
				"/export/home/teacher_file",
				"教师素材路径",
				1
				);
		try {
			iTeacherDictionariesDao.saveTeacherDictionaries(bean);
			logger.info(" list : {} " , iTeacherDictionariesDao.queryTeacherDictionariesList(bean));
		} catch (Exception e) {
			logger.error("" , e);
		}
	}
}