package com.tcs.test.ftp;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcs.util.ftp.FtpUtil;
import com.tcs.util.zip.ZipUtil;

public class FtpClientTest {

	private final Logger logger = LoggerFactory.getLogger(FtpClientTest.class);

	private final String fileName = "D:\\tempFileOfFTP\\input.txt";

	private OutputStream output = null;

	private BufferedOutputStream bos = null;
	
	private File file;

	@Before
	public void go() {
		try {
			long startTime = System.currentTimeMillis();
			logger.debug(" 生成文件!");
			output = new FileOutputStream(fileName);
			bos = new BufferedOutputStream(output);
			for (int i = 0; i < 1048576; i++) {
				bos.write("hello word hello hadoop hello ecipse hello bigdata".getBytes());
				bos.write("\r\n".getBytes());
			}
			bos.flush();
			long endTime = System.currentTimeMillis();
			logger.debug(" 生成压缩文件!");
			ZipUtil.ZipFiles(this.getZipFile("D:\\tempFileOfFTP\\").listFiles(), this.getZipFile("D:\\input.tar"));
			logger.debug(" 生成压缩文件结束!");
			logger.debug(" 共耗时 : {} " , endTime - startTime);
		} catch (FileNotFoundException e) {
			logger.error("创建文件失败", e);
		} catch (IOException e) {
			logger.error("写入数据失败,或者生成ZIP 发生异常 ", e);
		} finally {
			try {
				bos.close();
				output.close();
			} catch (IOException e) {
				logger.error("关闭写流失败,原因", e);
			}
		}
	}

	@Test
	public void upload() {
		try {
			logger.debug(" 开始上传tar!");
			file = new File("D:\\input.tar");
			FileInputStream in = new FileInputStream(file);
			boolean flag = FtpUtil.uploadFile("master", 21, "ftp_wangbo", "linkage!^*", "/write",
					"", "input.zip", in);
			logger.debug(" 是否成功上传 : {} " , flag);
		} catch (FileNotFoundException e) {
			logger.error("" , e);
		} finally {
			if (file.exists()) {
				boolean isdel = file.delete();
				logger.debug(" 删除本地文件成功! :  {}" , isdel);
			}
		}
	}
	
	private File getZipFile(String zipFilePath) {
		File file = null;
		if (null != zipFilePath && !zipFilePath.isEmpty()) {
			file = new File(zipFilePath);
		}
		return file;
	}
}