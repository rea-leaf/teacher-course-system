package com.tcs.test.print;

import java.util.Date;

import org.joda.time.DateTime;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PrintTest {

	private Logger logger = LoggerFactory.getLogger(PrintTest.class);
	
	@Test
	public void print() {
//		int btyeLength = "hello word hello hadoop hello ecipse hello bigdata".getBytes().length;
//		long Msize = 1024 * 1024 * 50;
//		long result = Msize / btyeLength;
//		logger.info(" length : {} , result : {} , Msize : {} " , btyeLength , result , Msize);
//		long i = 0;
//		while (true) {
//			System.out.println(i++);
//		}
		
//		String[] array = null;
//		List<String> list = null;
//		try {
//			list = Arrays.asList(array);
//			logger.info("list : {}" , list);
//		} catch (Exception e) {
//			logger.error("" , e);
//		}
//		logger.info(" nihao !");
//		array = (String[]) list.toArray();
//		logger.info("array : {}" , Arrays.toString(array));
		
//		MonitoringDetalBean monitoringBean = new MonitoringDetalBean("1","ip","","");
//		logger.info(" monitoringBean : {} " , monitoringBean);
		
//		String params = "1 $ 2 $";
//		String [] reuslt = params.split("\\$");
//		logger.info(" params : {} , result : {}  " , Arrays.toString(reuslt) , reuslt.length);
		
//		logger.info(" {} : " , this.getClass().getResource("/"));
		
//		logger.info("date : {} " , new Date("2018-03-08").getTime() / 1000);
		
		try {
			DateTime dateTime = new DateTime(new Date());
			logger.info(" date : {} , \n toString : {} \n yyyyMMdd : {} " , dateTime.getHourOfDay() , dateTime.toString("yyyy-MM-dd"), "http://api.goseek.cn/Tools/holiday?date=" + dateTime.toString("yyyyMMdd"));
		} catch (Exception e) {
			logger.error("" , e);
		}
	}
}
