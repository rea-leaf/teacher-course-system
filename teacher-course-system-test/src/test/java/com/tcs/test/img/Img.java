package com.tcs.test.img;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

public class Img {
	
	private Logger logger = LoggerFactory.getLogger(Img.class);
	
	@Test
	public void soImg() {
		try {
			URI uri = new URI("http://ip.zdaye.com/img/m_8e6dbaa338e1cab3.gif?2018/7/23%2011:09:59");
            File imageFile = new File("C:\\Users\\Administrator\\Desktop\\111.png");//图片位置
            ITesseract instance = new Tesseract();
//            instance.setDatapath("D:\\Tess4J-3.4.8-src\\Tess4J");
//            instance.setLanguage("eng");//选择字库文件（只需要文件名，不需要后缀名）
            String result = instance.doOCR(imageFile);//开始识别
            logger.info("result : {}" , result);//打印图片内容
        } catch (TesseractException e) {
        	logger.error("" , e);
        } catch (URISyntaxException e) {
        	logger.error("" , e);
		} catch (Exception e) {
			logger.error("" , e);
		}
	}
}
