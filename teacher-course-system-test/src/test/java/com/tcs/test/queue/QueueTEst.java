package com.tcs.test.queue;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QueueTEst {

	private Logger logger = LoggerFactory.getLogger(QueueTEst.class);

	private LinkedBlockingQueue<String> queue = new LinkedBlockingQueue<String>(50);

	class Fang implements Runnable {

		@Override
		public void run() {
			try {
				while (true) {
					logger.info("放入 {} ", queue.size());
					queue.put("1111");
				}
			} catch (InterruptedException e) {
				logger.error("", e);
			}
		}
	}

	class Qu implements Runnable {

		@Override
		public void run() {
			try {
				while (true) {
					logger.info(" msg : {} ", queue.take());
				}
			} catch (InterruptedException e) {
				logger.error("", e);
			}
		}
	}

	public static void main(String[] args) {
		ExecutorService exService = Executors.newFixedThreadPool(2);
		QueueTEst t = new QueueTEst();
		exService.execute(t.new Fang());
		exService.execute(t.new Qu());
	}

}
