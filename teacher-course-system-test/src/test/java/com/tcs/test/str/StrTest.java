package com.tcs.test.str;

import java.text.SimpleDateFormat;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StrTest {
	
	private final Logger logger = LoggerFactory.getLogger(StrTest.class);

	private final String value = "2018-04-07 16:06:46,385 1699120 , 149880 , 0.04 , 1523088406385 , slave2 , 192.168.1.114";
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	
	public void test() {
		String temp,v = "";
		StringBuffer sb = new StringBuffer();
		try {
			temp = value.substring(0 , value.indexOf(","));
			sb.append(temp).append(",");
			temp = sdf.parse(temp).getTime() / 1000 + "";
			sb.append(temp).append(",");
			logger.info(" sb : {} " , sb.toString());
			v = value.substring(23 , value.length() - 1);
			logger.info("str : {} , v : {}" , temp , v);
		} catch (Exception e) {
			logger.error("" , e);
		}
	}
	
	@Test
	public void test1() {
//		for (int i = 0; i < 10; i++) {
//			if (i % 2 == 0) {
//				logger.info(" i % 2 : {} " , i);
//				continue;
//			}
//			logger.info(" i : {} " , i);
//		}
		logger.info("length : {} " , "2018-04-06 00:17:22,1522945042,, 3540680 , 332308 , 0.0196078431372549 , 1522945042471 , slave1 , 192.168.1.113".length());
	}
}
