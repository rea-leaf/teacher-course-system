package com.tcs.test.hdfs;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.mapred.JobConf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 本地测试
 * 
 * @author wangbo
 */
public class HDFSApi {

	private final Logger logger = LoggerFactory.getLogger(HDFSApi.class);

	// 修改成自己的HDFS主机地址
	private static final String HDFS = "hdfs://master:9000/";

	private HDFSApi hdfsApi = null;

	private String hdfsPath;

	private Configuration conf;

	public HDFSApi() {
	};

	/**
	 * 两个构造器
	 * 
	 * @param conf
	 */
	// public HDFSApi(Configuration conf) {
	// this(HDFS, conf);
	// }

	public HDFSApi(String hdfs, Configuration conf) {
		this.hdfsPath = hdfs;
		this.conf = conf;
	}

	public JobConf config() {
		JobConf jobConf = new JobConf();
		jobConf.setJobName("hdfs_test");
		return jobConf;
	}

	/**
	 * 创建目录
	 * 
	 * @param folder
	 * @throws IOException
	 */
	public void mkdir(String folder) throws IOException {
		Path path = new Path(folder);
		FileSystem fs = FileSystem.get(URI.create(hdfsPath), conf);
		if (!fs.exists(path)) {
			fs.mkdirs(path);
			logger.debug("Create: {} " + folder);
		} else {
			logger.debug("文件已经存在!");
		}
		fs.close();
	}

	/**
	 * 上传文件
	 * 
	 * @param src
	 * @param dst
	 * @throws IOException
	 */
	public void updateFile(String src, String dst) throws IOException {
		FileSystem fs = FileSystem.get(URI.create(hdfsPath), conf);
		try {
			fs.copyFromLocalFile(false, new Path(src), new Path(dst));
			logger.debug(" 上传成功!");
		} catch (IllegalArgumentException e) {
			logger.error(" 上传失败!", e);
		} finally {
			fs.close();
		}
	}

	/**
	 * 删除HDFS文件索引
	 * 
	 * @param path
	 * @throws IOException
	 */
	public boolean removeFile(String path) throws IOException {
		Boolean result = true;
		Path dstPath = new Path(path);
		FileSystem fs = FileSystem.get(URI.create(hdfsPath), conf);
		try {
			result = fs.deleteOnExit(dstPath);
		} catch (Exception e) {
			logger.error(" 删除 文件索引 : ", e);
		} finally {
			logger.debug(" 删除 : {}  , 状态 :  {} !", path, result);
			fs.close();
		}
		return result;
	}

	/**
	 * 重命名文件
	 * 
	 * @param src
	 * @param dst
	 * @throws IOException
	 */
	public boolean rename(String src, String dst) throws IOException {
		Boolean result = true;
		FileSystem fs = FileSystem.get(URI.create(hdfsPath), conf);
		try {
			Path name1 = new Path(src);
			Path name2 = new Path(dst);
			result = fs.rename(name1, name2);
		} catch (IllegalArgumentException e) {
			logger.error("", e);
		} finally {
			logger.info("Rename: from  {} , to : {} , result : {} ", src, dst, result);
			fs.close();
		}
		return result;
	}

	/**
	 * 查看文件中的内容
	 * 
	 * @param remoteFile
	 * @return
	 * @throws IOException
	 */
	public String cat(String remoteFile) throws IOException {
		String str = null;
		Path path;
		FileSystem fs = null;
		FSDataInputStream fsdis = null;
		OutputStream baos;
		try {
			path = new Path(remoteFile);
			logger.info("cat:  {} , hdfsPath : {}  " , remoteFile , hdfsPath);
			fs = FileSystem.get(URI.create(hdfsPath), conf);
			fsdis = null;
			baos = new ByteArrayOutputStream();
			fsdis = fs.open(path);
			IOUtils.copyBytes(fsdis, baos, 4096, false);
			str = baos.toString();
		} catch (IllegalArgumentException e) {
			logger.error("" , e);
		} finally {
			IOUtils.closeStream(fsdis);
			fs.close();
		}
		return str;
	}

	public static void main(String[] args) {
		new HDFSApi().test1();
	}

	public void test1() {
		this.init();
		try {
//			hdfs.mkdir("/sogou");
//			hdfs.updateFile("D:\\test.txt", "/test1");
			hdfsApi.updateFile("/home/wangbo/sogou.500w.utf8", "/sogou.500w.utf8");
//			hdfs.rename("/test1/test.txt", "/test1/test2.txt");
			// hdfs.removeFile("/test1");
//			String result = hdfsApi.cat("/test1/test2.txt");
//			byte [] byteArray = result.getBytes();
//			logger.info(" result : --------------------------- {} " , result);
		} catch (IOException e) {
			logger.error("", e);
		}
	}

	public void init() {
		JobConf conf = config();
		hdfsApi = new HDFSApi(HDFS, conf);
	}
}