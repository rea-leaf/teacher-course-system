package com.tcs.test.executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExecutorTest {

	private final Logger logger = LoggerFactory.getLogger(ExecutorTest.class);

	private ExecutorService executorService = Executors.newFixedThreadPool(4);

	private ResultBean task;
	
	private LinkedBlockingQueue<Future<Boolean>> queue = new LinkedBlockingQueue<Future<Boolean>>(20);
	
	public static void main(String [] args) {
		Thread t1 = new Thread(new ExecutorTest().new PutTask() , " put Thread");
		Thread t2 = new Thread(new ExecutorTest().new TakeTask() , "take Thread");
		t1.start();
		t2.start();
	}
	
	public class PutTask implements Runnable {
		
		int index = 0;
		
		public PutTask() {};
		
		public PutTask(int index) {
			super();
			this.index = index;
		}

		@Override
		public void run() {
			while (true) {
				task = new ResultBean(index++);
				Future<Boolean> future = executorService.submit(task);
				try {
					if (!queue.offer(future)) {
						queue.put(future);
					}
				} catch (InterruptedException e) {
					logger.error("", e);
				} catch (Exception e) {
					logger.error("", e);
				}
				try {
					Thread.sleep(1000L * 2);
				} catch (InterruptedException e) {
					logger.error("", e);
				}
			}
		}
	}
	
	public class TakeTask implements Runnable {
		
		boolean flag = false;
		public void run() {
			while (true) {
				Future<Boolean> future = null;
				try {
					future = queue.poll();
					if (null == future) {
						logger.debug("future : {} , size : {}  " , future , queue.size());
						future = queue.take();
					}
				} catch (Exception e) {
					logger.error("", e);
				}
				try {
					if (flag) {
						future.cancel(flag);
					}
					flag = future.get();
				} catch (InterruptedException e) {
					logger.error("", e);
				} catch (Exception e) {
					logger.error("", e);
				}
			}
		}
	}
}