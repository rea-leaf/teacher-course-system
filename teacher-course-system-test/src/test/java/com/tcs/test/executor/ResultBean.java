package com.tcs.test.executor;

import java.util.concurrent.Callable;

public class ResultBean implements Callable<Boolean> {
	
	private int index = 0;
	
	public ResultBean() {};
	
	public ResultBean(int index) {
		this.index = index;
	}

	@Override
	public Boolean call() throws Exception {
		return index < 5 ? false : true;
	}
}
