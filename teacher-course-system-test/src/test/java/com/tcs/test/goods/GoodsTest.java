package com.tcs.test.goods;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.internal.Lists;
import com.tcs.model.goods.GoodsBean;
import com.tcs.model.goods.GoodsParamsBean;
import com.tcs.model.goods.RegionBean;
import com.tcs.model.goods.RegionGoodsBean;
import com.tcs.server.goods.IGoodsServer;
import com.tcs.test.BaseJunit4Test;
import com.tcs.util.uuid.UUIDUtil;

/**
 * 货物测试
 * @author wangbo
 */
public class GoodsTest extends BaseJunit4Test {
	
	private final Logger logger = LoggerFactory.getLogger(GoodsTest.class);
	
	@Resource(name = "iGoodsServer")
	private IGoodsServer iGoodsServer;
	
	public void add() {
		String [] regionArray = {"A","B","C","D","E"};
		RegionBean regionBean = null;
		List<RegionBean> regionList = Lists.newArrayList();
		for (int i = 0; i < regionArray.length; i++) {
			regionBean = new RegionBean(regionArray[i],UUIDUtil.getUUID());
			regionList.add(regionBean);
		}
		iGoodsServer.addRegionList(regionList);
		logger.info(" list :{} " , regionList);
	}
	
	public void say() {
		GoodsBean goodsBean = null;
		List<GoodsBean> goodsList = Lists.newArrayList();
		RegionGoodsBean regionGoodsBean = null;
		List<RegionGoodsBean> RegionGoodsBeanList = Lists.newArrayList();
		String regionId = "0db808297351459683cd30b6ca676390";
		String goods = "红苹果,苹果,梨";
		try {
			String [] goodsArray = goods.split(",");
			for (String tempGoods : goodsArray) {
				goodsBean = new GoodsBean(tempGoods,0,UUIDUtil.getUUID());
				goodsList.add(goodsBean);
			}
			iGoodsServer.addGoodsList(goodsList);
			// 区域,货物 关系
			for (GoodsBean tempGoodsBean : goodsList) {
				regionGoodsBean = new RegionGoodsBean(tempGoodsBean.getId(), regionId, 0, UUIDUtil.getUUID());
				RegionGoodsBeanList.add(regionGoodsBean);
			}
			iGoodsServer.addGoodsRegionList(RegionGoodsBeanList);
		} catch (Exception e) {
			logger.error("" , e);
		}
	}
	
	@Test
	public void query() {
//		logger.info(" info : {} " , iGoodsServer.queryRegionGoodList(new GoodsParamsBean("A", 1520179200, 1520265599)));
		GoodsParamsBean g = new GoodsParamsBean();
		g.setGoodsName("11111");
		g.setRegionId("22222");
//		g.setDate("2018-03-08");
//		g.setId("1047fa97db174787b003f3ec9568df2c");
		g.setStatus(1);
//		logger.info(" info : {} " , iGoodsServer.queryCaiGouGoodsListByDate(g));
		logger.info(" info : {} " , iGoodsServer.addGoodsByParams(g));
//		int result = 0;
//		try {
//			GoodsParamsBean g = new GoodsParamsBean("1", 0,0, "1",0,"1");
////			result = iGoodsServer.addGoodsById(g);
//			
//			result = iGoodsServer.removeGoodsById(g);
//			logger.info(" result : {}" , result);
//		} catch (Exception e) {
//			logger.error( "" , e);
//		}
	}
}