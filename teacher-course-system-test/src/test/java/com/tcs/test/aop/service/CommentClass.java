package com.tcs.test.aop.service;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.tcs.redis.annotations.RedisCache;
import com.tcs.util.http.HttpClientUtil;

@Component(value="commentClass")
public class CommentClass {
	
	private final Logger logger = LoggerFactory.getLogger(CommentClass.class);
	
	private DateTime dateTime = new DateTime(new Date());
	
	@RedisCache(saveTime=1000 * 10)
	public List<Map<String,String>> say(String params,int age,Map<String,String> parmasMap) {
		Map<String,String> resultMap = new HashMap<String,String>();
		List<Map<String,String>> p = new ArrayList<Map<String,String>>();
		resultMap.put("one", "one");
		resultMap.put("two", "two");
		resultMap.put("three", "three");
		System.err.println(" 执行了执行了执行了执行了执行了执行了执行了执行了执行了执行了执行了执行了执行了执行了执行了执行了执行了!!!");
		p.add(resultMap);
		return p;
	}
	
	@RedisCache(saveTime=5,timeUnit=TimeUnit.SECONDS)
	public int getDate() {
		int result = 0;
		try {
			result = NumberUtils.toInt(HttpClientUtil.get(getUrl("http://api.goseek.cn/Tools/holiday?date=",dateTime.toString("yyyyMMdd"))));
		} catch (KeyManagementException e) {
			logger.error("" , e);
		} catch (NoSuchAlgorithmException e) {
			logger.error("" , e);
		} catch (KeyStoreException e) {
			logger.error("" , e);
		} catch (IOException e) {
			logger.error("" , e);
		}
		return result;
	}
	
	/**
	 * 
	* @Title: getUrl  
	* @Description: 组装URL
	* @param url
	* @param params
	* @return {@link String}
	 */
	private String getUrl(String url,Object params) {
		StringBuffer urlSb = new StringBuffer(url).append(params.toString());
		return urlSb.toString();
	}
}