package com.tcs.test.aop.main;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcs.test.BaseJunit4Test;
import com.tcs.test.aop.service.CommentClass;

public class AopStartUp extends BaseJunit4Test {
	
	private final Logger logger = LoggerFactory.getLogger(AopStartUp.class);
	
	@Resource(name="commentClass")
	private CommentClass commentClass;

	@Test
	public void runGo() {
		Map<String,String> paramsMap = new HashMap<String,String>();
		paramsMap.put("name", "wangbo");
		try {
			logger.debug("commentClass.say() : {} " , commentClass.say("ni hao!" , 1 , paramsMap));
		} catch (Exception e) {
			logger.error("" , e);
		}
	}
}