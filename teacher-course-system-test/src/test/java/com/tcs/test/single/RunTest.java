package com.tcs.test.single;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.tcs.util.spring.SpringConfigTool;

public class RunTest {
	
	ExecutorService executorService = Executors.newFixedThreadPool(6);
	
	public static void main(String[] args) {
		new ClassPathXmlApplicationContext("classpath:spring/app-test.xml").start();
		new RunTest().run();
	}

	public void run() {
		for (int i = 0; i < 99999999; i++) {
			executorService.execute(new Run1());
		}
	}
	
}

class Run1 implements Runnable {
	
	@Override
	public void run() {
		SpringConfigTool.init();
	}
}
