package com.tcs.test.hive;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * hive java api 
 * @author wangbo
 *
 */
public interface IHiveApi {

	/**
	 * 初始化
	 */
	void init();
	
	/**
	 * 连接
	 * @param url
	 */
	Connection getConnection(String url) throws SQLException;
}
