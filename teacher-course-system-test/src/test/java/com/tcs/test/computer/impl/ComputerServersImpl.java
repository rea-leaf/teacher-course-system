package com.tcs.test.computer.impl;

import com.tcs.test.computer.ComputerServers;

/**
 * 实现类
 * @author wangBo
 *
 */
public class ComputerServersImpl implements ComputerServers {

	@Override
	public double add(double val1, double val2) {
		return val1 + val2;
	}

	@Override
	public double minus(double val1, double val2) {
		return val1 - val2;
	}

}
