package com.tcs.test.computer;

/**
 * 接口
 * @author wangBo
 */
public interface ComputerServers {
	
	/**
	 * 加法
	 * @param val1
	 * @param val2
	 * @return
	 */
	double add(double val1 , double val2);
	
	/**
	 * 减法
	 * @param val1
	 * @param val2
	 * @return
	 */
	double minus(double val1 , double val2);

}
