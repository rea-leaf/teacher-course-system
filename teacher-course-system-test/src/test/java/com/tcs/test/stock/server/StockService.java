package com.tcs.test.stock.server;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import org.joda.time.DateTime;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcs.util.http.HttpClientUtil;

public class StockService {
	
	private final Logger logger = LoggerFactory.getLogger(StockService.class);
	
	private DateTime dateTime = new DateTime(new Date());
	
	public void initRedis() {
		
	}
	
	@Test
	public void http() {
		try {
			String holiday = HttpClientUtil.get(getUrl("http://api.goseek.cn/Tools/holiday?date=",dateTime.toString("yyyyMMdd")));
			logger.debug(" holiday : {} " , holiday);
		} catch (KeyManagementException e) {
			logger.error("" , e);
		} catch (NoSuchAlgorithmException e) {
			logger.error("" , e);
		} catch (KeyStoreException e) {
			logger.error("" , e);
		} catch (IOException e) {
			logger.error("" , e);
		}
	}
	
	/**
	 * 
	* @Title: getUrl  
	* @Description: 组装URL
	* @param url
	* @param params
	* @return {@link String}
	 */
	private String getUrl(String url,Object params) {
		StringBuffer urlSb = new StringBuffer(url).append(params.toString());
		return urlSb.toString();
	}
}