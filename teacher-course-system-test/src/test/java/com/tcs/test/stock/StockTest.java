package com.tcs.test.stock;

import javax.annotation.Resource;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcs.dao.dist.IDistDao;
import com.tcs.dao.stock.IStockDao;
import com.tcs.server.rule.IRuleService;
import com.tcs.server.stock.IStockService;
import com.tcs.test.BaseJunit4Test;

public class StockTest extends BaseJunit4Test {

	private Logger logger = LoggerFactory.getLogger(StockTest.class);

	@Resource(name = "iStockService")
	private IStockService iStockService;
	
	@Resource(name = "iRuleService")
	private IRuleService iRuleService;
	
	@Resource(name="iStockDao")
	private IStockDao istockDao;
	
	@Resource(name="iDistDao")
	private IDistDao distDao;
	
	@Test
	public void run() {
		logger.info(" nihao : {} " , distDao.getValueByKey("mogu"));
	}
}