package com.tcs.test.http;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpClientTest {
	
	private final Logger logger = LoggerFactory.getLogger(HttpClientTest.class);
	
	private String getURL1(String url,String [] params) {
		HttpClient client = null;
		String result = "";
		try {
			client = new HttpClient();// 定义client对象
			client.getHttpConnectionManager().getParams().setConnectionTimeout(10 * 1000);// 设置连接超时时间为2秒（连接初始化时间）
			GetMethod method = new GetMethod(url);// 访问下谷歌的首页
			int statusCode = client.executeMethod(method);// 状态，一般200为OK状态，其他情况会抛出如404,500,403等错误
			if (statusCode != HttpStatus.SC_OK) {
				System.out.println("远程访问失败。");
			}
			result =  method.getResponseBodyAsString();
		} catch (HttpException e) {
			logger.error(url ,e);
		} catch (IOException e) {
			logger.error(url ,e);
		} finally {
			client.getHttpConnectionManager().closeIdleConnections(1);
		}
		return result;
	}
	
	@Test
	public void test1() {
		String result = HttpClientUtil.get("http://weixin.xagszb.com/sisuers_web/a/Content/detail?id=3702");
		Document document = Jsoup.parse(result);
		Elements titles = document.select("title");
		logger.info("titles : {} " , titles.html());
		Elements es = document.getElementsByTag("head");
		Elements main = document.select("div.main");
		logger.info("main : {} " , main.text());
	}
	
	public void test11() {
		int index = 1;
		String result = "";
		while (index <= 337) {
			try {
				Thread.sleep(1000L * 10);
			} catch (InterruptedException e) {
			}
//			result = this.getURL1("http://www.bjsat.gov.cn/bjsat/office/jsp/qsgg/query.jsp?page_num="+ index +"&dwmc=&fbdw=%B7%E1%CC%A8&nsrlx=%C6%F3%D2%B5%BB%F2%B5%A5%CE%BB&BeginTime=2017-01-01&EndTime=2017-11-23&nsrsbh=&fzrxm=&zjhm=", null);
			result = HttpClientUtil.get("http://www.bjsat.gov.cn/bjsat/office/jsp/qsgg/query.jsp?page_num="+ index +"&dwmc=&fbdw=%B7%E1%CC%A8&nsrlx=%C6%F3%D2%B5%BB%F2%B5%A5%CE%BB&BeginTime=2017-01-01&EndTime=2017-11-23&nsrsbh=&fzrxm=&zjhm=");
			logger.info("result: {} " , result);
			index++;
			if (result.indexOf("天儒") > -1) {
				logger.info(" 命中 : {} ,第几页 : {} " , result , index);
			}
		}
	}
}
