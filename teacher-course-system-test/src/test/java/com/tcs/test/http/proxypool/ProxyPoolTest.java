package com.tcs.test.http.proxypool;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.internal.Lists;

public class ProxyPoolTest {

	private static final Logger logger = LoggerFactory.getLogger(ProxyPoolTest.class);

	private List<ProxyInfo> resultList = Lists.newArrayList();

	private Map<String, String> concurrentMap = new HashMap<String, String>();

	private ExecutorService executorService = Executors.newFixedThreadPool(4);
			//newFixedThreadPool(4);
			//newCachedThreadPool(new SimpleThreadFactory());

	private String url = "http://www.xicidaili.com/nn/";

	public static void main(String[] args) {
//		for (int i = 1; i < 100; i++) {
//			logger.info(" i : {} , % : {} " , i , i % 4);
//		}
		new ProxyPoolTest().run();
	}

	public void run() {
		boolean runFalg = true;
		int index = 0;
		CountDownLatch countDownLatch = new CountDownLatch(4);
		MyTh myTh = null;
		while (runFalg) {
			myTh = new MyTh(++index, url, resultList, concurrentMap, countDownLatch);
			executorService.execute(myTh);
			if (index != 1 && index % 4 == 1) {
				try {
					countDownLatch.await();
				} catch (InterruptedException e) {
					logger.error("", e);
				} finally {
					countDownLatch = new CountDownLatch(4);
				}
			}
		}
		logger.info(" resultList : {}  ", resultList);
	}
}

class SimpleThreadFactory implements ThreadFactory {
	public Thread newThread(Runnable r) {
		return new Thread(r);
	}
}

class MyTh implements Runnable {

	private final Logger logger = LoggerFactory.getLogger(MyTh.class);

	String ipReg = "\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3} \\d{1,6}";

	Pattern ipPtn = Pattern.compile(ipReg);

	private Map<String, String> concurrentMap;

	private List<ProxyInfo> resultList;

	private CountDownLatch countDownLatch;

	private int index;

	private String url;

	public MyTh() {
	};

	public MyTh(int index, String url, List<ProxyInfo> resultList, Map<String, String> concurrentMap,
			CountDownLatch countDownLatch) {
		this.index = index;
		this.url = url;
		this.resultList = resultList;
		this.concurrentMap = concurrentMap;
		this.countDownLatch = countDownLatch;
	}

	@Override
	public void run() {
		try {
			String[] strs = null;
			Document document = Jsoup
					.connect(url + index)
					.userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36")
					.header("Host", "www.xicidaili.com")
					.get();
			Matcher m = ipPtn.matcher(document.text());
			while (m.find()) {
				strs = m.group().split(" ");
				logger.info(" strs : {} , url : {}  ", Arrays.toString(strs), url + index);
				if (checkProxy(strs[0], Integer.parseInt(strs[1]))) {
					logger.info("获取到可用代理IP : {} prot : {}", strs[0], strs[1]);
					// concurrentMap.put(strs[0] + strs[1], "");
					// resultList.add(new ProxyInfo(strs[0],strs[1],"http"));
				}
			}
		} catch (IOException e) {
			logger.error("", e);
		} finally {
			this.countDownLatch.countDown();
		}
	}

	/**
	 * 判断ip和端口是否有效
	 * 
	 * @param ip
	 * @param port
	 * @return
	 */
	private static boolean checkProxy(String ip, Integer port) {
		try {
			// http://1212.ip138.com/ic.asp 可以换成任何比较快的网页
			Jsoup.connect("http://1212.ip138.com/ic.asp").timeout(2 * 1000).proxy(ip, port).get();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}

class ProxyInfo {
	private String userName = "";
	private String ip;
	private String password = "";
	private String type;
	private String port;
	private int is_internet = 1;

	public ProxyInfo(String ip, String port, String type) {
		this.ip = ip;
		this.type = type;
		this.port = port;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public int getIs_internet() {
		return is_internet;
	}

	public void setIs_internet(int is_internet) {
		this.is_internet = is_internet;
	}
}