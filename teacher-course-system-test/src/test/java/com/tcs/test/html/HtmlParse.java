package com.tcs.test.html;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HtmlParse {
	
	private final Logger logger = LoggerFactory.getLogger(HtmlParse.class);

	public String getHtml() {
		StringBuffer html = new StringBuffer(500);
		BufferedReader input = null;
		try {
			input = new BufferedReader(new InputStreamReader(new FileInputStream("G:\\eclipse-workspace\\teacher-course-system\\teacher-course-system-test\\src\\test\\resources\\config\\demo.txt"), "UTF-8"));
			String line = "";
			while ((line = input.readLine()) != null) {
				html.append(line);
			}
		} catch (FileNotFoundException e) {
			logger.error("" , e);
		} catch (IOException e) {
			logger.error("" , e);
		} finally {
			try {
				input.close();
			} catch (IOException e) {
				logger.error("" , e);
			}
		}
		return html.toString();
	}
	
	@Test
	public void test1() {
		String result = this.getHtml();
		try {
			Document document = Jsoup.parse(result);
			Elements titles = document.select("title");
			logger.info("titles : {} " , titles.html());
			Elements es = document.getElementsByTag("head");
			Elements main = document.select(".tableMain");
			String isOrNull = main.select("ul.tableContain span.tableTdVal").first().html();
			if (StringUtils.isNotBlank(isOrNull)) {
				logger.debug(" outerHtml : {}" , main.outerHtml());
			}
//			logger.info("main : {} " , main.html().replaceAll("<p>", "").replaceAll("</p>", "").replaceAll("&nbsp;", ""));
//			for (Element element : es) {
//				logger.info(" element.txt : {} , html : {} , es. length : {} " , element.text() , element.html() , es.size());
//			}
		} catch (Exception e) {
			logger.error("" , e);
		}
	}
}