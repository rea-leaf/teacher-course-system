package com.tcs.test.kafka.producer;

import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaProducerTest {

	private final Logger logger = LoggerFactory.getLogger(KafkaProducerTest.class);

	public static void main(String[] args) {
		KafkaProducerTest kafka = new KafkaProducerTest();
		ExecutorService executorService = Executors.newFixedThreadPool(2);
//		executorService.execute(kafka.new ProducerTest());
		executorService.execute(kafka.new ConsumerTest("group1"));
		executorService.execute(kafka.new ConsumerTest("group2"));
	}

	class ProducerTest implements Runnable {

		@Override
		public void run() {
			Properties props = new Properties();
			props.put("bootstrap.servers", "192.168.1.111:9092,192.168.1.113:9092,192.168.1.114:9092");
			props.put("acks", "all");
			props.put("retries", 0);
			props.put("batch.size", 16384);
			props.put("linger.ms", 1);
			props.put("buffer.memory", 33554432);
			props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
			props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
			ProducerRecord<String, String> record = null;

			Producer<String, String> producer = new KafkaProducer<String, String>(props);
			int partition = 0;
			while (true) {
				try {
					record = new ProducerRecord<String, String>("log", partition, String.valueOf(new Date().getTime()),
							String.valueOf(new Date().getTime()));
					// logger.info(" topic : {} , partition : {}" , record.topic() ,
					// record.partition());
					producer.send(record);
					if (++partition == 3) {
						partition = 0;
					}
				} catch (Exception e) {
					logger.error("", e);
					producer.close();
					System.exit(1);
				}
			}

		}
	}

	class ConsumerTest implements Runnable {
		
		private String groupId;
		
		public ConsumerTest() {};
		
		public ConsumerTest(String groupId) {
			this.groupId = groupId;
		}

		@Override
		public void run() {
			Properties props = new Properties();
			props.put("bootstrap.servers", "192.168.1.111:9092");
			props.put("group.id", groupId);
			props.put("enable.auto.commit", "true");
			props.put("auto.commit.interval.ms", "1000");
			props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
			props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
			KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
			consumer.subscribe(Arrays.asList("log"));
			while (true) {
//				try {
//					Thread.sleep(1 * 1000L);
//				} catch (InterruptedException e) {
//					logger.error("", e);
//				}
				ConsumerRecords<String, String> records = consumer.poll(100);
				for (ConsumerRecord<String, String> record : records) {
					logger.error(" grpupId: {} topic : {} , partition : {} , offset : {}, value : {} ", this.groupId, record.topic(),
							record.partition(), record.offset(), record.value());
				}
			}

		}
	}

}
