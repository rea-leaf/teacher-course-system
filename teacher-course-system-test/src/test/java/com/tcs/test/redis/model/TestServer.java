package com.tcs.test.redis.model;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tcs.redis.annotations.RedisCache;

@Component(value="testServer")
public class TestServer {
	
	@RedisCache(saveTime=1,timeUnit=TimeUnit.MINUTES)
	public List<List<TestModel>> queryTestModel() {
		System.err.println(" 走业务!!!!  ");
		List<List<TestModel>> oneList = Lists.newArrayList();
		List<TestModel> resultList = Lists.newArrayList();
		resultList.add(new TestModel(11));
		oneList.add(resultList);
		return oneList;
	}
	
	@RedisCache(saveTime=1,timeUnit=TimeUnit.MINUTES)
	public Map<String,List<TestModel>> queryTestModel1() {
		Map<String,List<TestModel>> resultMap = Maps.newHashMap();
		System.err.println(" 走业务!!!!  ");
		List<TestModel> resultList = Lists.newArrayList();
		resultList.add(new TestModel(11));
		resultMap.put("one" , resultList);
		return resultMap;
	}
	
	@RedisCache(saveTime=24,timeUnit=TimeUnit.HOURS)
	public Map<String,List<List<TestModel>>> queryTestModel2() {
		Map<String,List<List<TestModel>>> resultMap = Maps.newHashMap();
		System.err.println(" 走业务!!!!  ");
		List<List<TestModel>> oneList = Lists.newArrayList();
		List<TestModel> resultList = Lists.newArrayList();
		resultList.add(new TestModel(11));
		oneList.add(resultList);
		resultMap.put("one" , oneList);
		return resultMap;
	}

}