package com.tcs.test.redis.model;

import java.io.Serializable;

public class TestModel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	public TestModel(){};

	public TestModel(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "TestModel [id=" + id + "]";
	}
}