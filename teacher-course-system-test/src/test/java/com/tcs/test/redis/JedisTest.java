package com.tcs.test.redis;

<<<<<<< HEAD
public class JedisTest {

}
=======
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

public class JedisTest {
	
	private final Logger logger = LoggerFactory.getLogger(JedisTest.class);
	
	JedisCluster jedisCluster = null;

    static String prefix = "luffi:lbl";
    static String KEY_SPLIT = ":"; //用于隔开缓存前缀与缓存键值

    String nameKey = prefix + KEY_SPLIT + "name";
	
	/**
     * 因为是测试，这里没有写单例
     */
    @Before
    public void before(){
        try {
			String[] serverArray = "192.168.1.111:7002,192.168.1.111:7003,192.168.1.113:7000,192.168.1.113:7001,192.168.1.114:7004,192.168.1.114:7005".split(",");
			Set<HostAndPort> nodes = new HashSet<>();

			for (String ipPort : serverArray) {
			    String[] ipPortPair = ipPort.split(":");
			    nodes.add(new HostAndPort(ipPortPair[0].trim(), Integer.valueOf(ipPortPair[1].trim())));
			}

			//注意：这里超时时间不要太短，他会有超时重试机制。而且其他像httpclient、dubbo等RPC框架也要注意这点
			jedisCluster = new JedisCluster(nodes, 1000, 1000, 1, "wswangbo007", new GenericObjectPoolConfig());

//        	大多数测试都是使用【nameKey】测试的，所以在启动之前先把这个key删掉
//			jedisCluster.del(nameKey);
		} catch (NumberFormatException e) {
			logger.error("" , e);
		} catch (Exception e) {
			logger.error("" , e);
		}
    }
    
    /**
     * 简单字符串读写
     */
    @Test
    public void setStringData(){
//        System.out.println(jedisCluster.set(nameKey, "张三"));
//        System.out.println(jedisCluster.get(nameKey));
    	
    	logger.debug(" com.tcs.reptile.service.impl.SeekServerImplisSeek[] : {} " , jedisCluster.get("com.tcs.reptile.service.impl.SeekServerImplisSeek[]"));
    }

    
}
>>>>>>> dug
