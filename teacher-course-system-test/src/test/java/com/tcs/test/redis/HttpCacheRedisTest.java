package com.tcs.test.redis;

import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcs.test.BaseJunit4Test;
import com.tcs.test.aop.service.CommentClass;

import redis.clients.jedis.JedisCluster;

public class HttpCacheRedisTest extends BaseJunit4Test {
	
	@Resource(name="jedisCluster")
	private JedisCluster jedisCluster;
	
	private final Logger logger = LoggerFactory.getLogger(HttpCacheRedisTest.class);
	
	@Resource(name="commentClass")
	private CommentClass commentClass;
	
	@Test
	public void httpTestCache() {
		for (int i = 0; i <= 3; i++) {
			try {
				logger.info("------------------- {} 请求" , i);
				commentClass.getDate();
				Thread.sleep(1000L * 4);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Test
	public void TimeTest() {
		long time = TimeUnit.SECONDS.toNanos(5);
		logger.info(" time : {} " , time);
		time = TimeUnit.MILLISECONDS.convert(5, TimeUnit.MINUTES);
		logger.info(" time : {} " , time);
	}

}