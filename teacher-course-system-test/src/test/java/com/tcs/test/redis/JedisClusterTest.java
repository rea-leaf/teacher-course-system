package com.tcs.test.redis;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.Resource;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcs.test.BaseJunit4Test;

import redis.clients.jedis.JedisCluster;

public class JedisClusterTest extends BaseJunit4Test {
	
	private final Logger logger = LoggerFactory.getLogger(JedisClusterTest.class);

	@Resource(name="jedisCluster")
	private JedisCluster jedisCluster;
	
	@Test
	public void firstTest() {
		jedisCluster.del("com.tcs.reptile.service.impl.SeekServerImplisSeek[]");
		logger.info(" com.tcs.reptile.service.impl.SeekServerImplisSeek[] : {} " , jedisCluster.get("com.tcs.reptile.service.impl.SeekServerImplisSeek[]"));
	}
	
	@Test
	public void incr() {
		jedisCluster.del("incrNum");
		CountDownLatch countDownLatch = new CountDownLatch(10);
		AtomicInteger atomicInteger = new AtomicInteger(0);
		for (int i = 0; i < 10; i++) {
			new Thread(new MyThread(atomicInteger,countDownLatch), " thread-" + i).start();
		}
		try {
			countDownLatch.await();
		} catch (InterruptedException e) {
			logger.error("" , e);
		}
		logger.info(" incrNum : {} , atomicInteger : {} " , jedisCluster.get("incrNum") , atomicInteger);
	}
	
	class MyThread implements Runnable {
		
		private AtomicInteger atomicInteger;
		
		private CountDownLatch countDownLatch;
		
		public MyThread() {};
		
		public MyThread(AtomicInteger atomicInteger , CountDownLatch countDownLatch) {
			this.atomicInteger = atomicInteger;
			this.countDownLatch = countDownLatch;
		}
		
		@Override
		public void run() {
			jedisCluster.incr("incrNum");
			atomicInteger.incrementAndGet();
			countDownLatch.countDown();
		}
	}
}
