package com.tcs.test.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Properties;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.support.PropertiesLoaderUtils;

public class LoadFileTest {

	private final Logger logger = LoggerFactory.getLogger(LoadFileTest.class);

	
	public void loadFileTest1() {
		Properties properties = new Properties();
		try {
			properties = PropertiesLoaderUtils.loadAllProperties("db/jdbc.properties");
		} catch (IOException e) {
			logger.error("", e);
		}
		for (Object key : properties.keySet()) {
			logger.info(" key : {}", key.toString());
			System.out.println(key.toString());
			logger.info("value : {}", properties.getProperty(key.toString()));
		}
	}
	
	@Test
	public void loadFileTest2() {
		Properties properties = new Properties();
		try {
			FileInputStream fileInputStream = new FileInputStream(new File("D:\\wangbo.properties"));;
//			URL url = new URL("D:/wangbo.properties");
//			InputStream inputStream = url.openStream();
			properties.load(fileInputStream);
			for (Object key : properties.keySet()) {
				logger.info(" key : {}", key.toString());
				System.out.println(key.toString());
				logger.info("value : {}", properties.getProperty(key.toString()));
			}
		} catch (MalformedURLException e) {
			logger.error("" , e);
		} catch (IOException e) {
			logger.error("" , e);
		}
	}
}