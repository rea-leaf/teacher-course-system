package com.tcs.test.email;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcs.model.email.MailSenderInfo;
import com.tcs.model.stock.StockBean;
import com.tcs.rpc.service.IEmailService;
import com.tcs.test.BaseJunit4Test;
import com.tcs.test.html.HtmlParse;
import com.tcs.util.uuid.UUIDUtil;

public class EmailTest extends BaseJunit4Test {
	
	private final Logger logger = LoggerFactory.getLogger(EmailTest.class);
	
	@Resource(name="iEmailService")
	private IEmailService iEmailService;
	
	private DateTime dateTime = new DateTime(new Date());
	
	private MailSenderInfo mailInfo;
	
	private HtmlParse htmlParse = new HtmlParse();
	
	private void setMailSenderInfo(StockBean stockBean) {
		mailInfo = new MailSenderInfo();
		mailInfo.setSubject(stockBean.getTitle());
		mailInfo.setSendFlag(2);	// HTML 格式
		mailInfo.setContent(stockBean.getTitle() + "<br>" + stockBean.getContent());
		mailInfo.setCcs(new String[]{"stantnks@gmail.com"});
	}
	
	public void work() {
		try {
			String html = htmlParse.getHtml();
			Document document = Jsoup.parse(html);
			document.select("img").remove();
			String title = dateTime.toString("yyy-MM-dd") + " | " + document.title();
			Elements main = document.select("div.main");
//			Elements img = document.select("img[src$=.png]");
//			logger.debug("html : {} \n , img : {} " , html , img.html(""));
			StockBean stockBean = null;
			if (StringUtils.isNotBlank(main.html())) {
				String content = main.html();
			    stockBean = new StockBean(UUIDUtil.getUUID(),title,content,0);
			    setMailSenderInfo(stockBean);
			    
			    iEmailService.send(mailInfo);
//				logger.debug("发送成功! 数据为 : {} " , mailInfo);
//			    logger.debug("html : {} , title : {} " , main.html() , title);
			}
//		} catch (KeyManagementException e) {
//			logger.error("" , e);
//		} catch (NoSuchAlgorithmException e) {
//			logger.error("" , e);
//		} catch (KeyStoreException e) {
//			logger.error("" , e);
//		} catch (IOException e) {
//			logger.error("" , e);
		} catch (Exception e) {
			logger.error("" , e);
		}
	}
	
	@Test
	public void runGo() {
		this.work();
	}
}
