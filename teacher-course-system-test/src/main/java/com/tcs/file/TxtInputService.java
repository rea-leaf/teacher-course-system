package com.tcs.file;

import java.util.List;

/**
 * 文本转换对象
 * @author wangBo
 * @param <T>
 */
public interface TxtInputService<T> {

	
	T conventBean(String txt);
	
	List<T> conventBeanList(String txt);
}
