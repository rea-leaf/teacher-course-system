package com.tcs.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcs.file.task.TaskRun;

public class MkdirFile {
	
	private final Logger logger = LoggerFactory.getLogger(MkdirFile.class);
	
	private Object lock = new Object();

	public static void main(String[] args) {
		new MkdirFile().run();
	}
	
	public void run() {
		logger.info("启动!");
		TaskRun taskRun = new TaskRun();
		new Thread(taskRun , " create_file_thread").start();
		try {
			lock.wait();
		} catch (InterruptedException e) {
			logger.error("" , e);
		}
	}
}
