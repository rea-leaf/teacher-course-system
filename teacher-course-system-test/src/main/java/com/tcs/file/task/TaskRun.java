package com.tcs.file.task;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TaskRun implements Runnable , Serializable {

	private static final long serialVersionUID = 1L;
	
	private final Logger logger = LoggerFactory.getLogger(TaskRun.class);
	
	private OutputStream outputStream = null;
	
	private BufferedOutputStream bos = null;
	
	@Override
	public void run() {
		try {
			outputStream = new FileOutputStream("/export/home/temp_file/input.txt");
			bos = new BufferedOutputStream(outputStream);
			for (int i = 0; i < 99999999; i++) {
				bos.write("hello word hello hadoop hello ecipse hello bigdata".getBytes());
				bos.write("\r\n".getBytes());
			}
			bos.flush();
		} catch (FileNotFoundException e) {
			logger.error("创建文件失败", e);
		} catch (IOException e) {
			logger.error("写入数据失败", e);
		} finally {
			try {
				bos.close();
				outputStream.close();
			} catch (IOException e) {
				logger.error("关闭写流失败,原因", e);
			}
		}
	}

}
