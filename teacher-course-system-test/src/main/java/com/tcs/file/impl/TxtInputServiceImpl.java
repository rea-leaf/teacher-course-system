package com.tcs.file.impl;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.lang.StringUtils;

public class TxtInputServiceImpl {

	private String sql;

	private String path = "F:\\input.txt";
	
	private StringBuffer sb = new StringBuffer("insert into temp_table ( id , username    , password , sex , content, datetime   , vm_id , isad) values ");

	public void readFileAtPath() throws IOException {
		if (StringUtils.isNotEmpty(path)) {
			FileInputStream fis = new FileInputStream(path);
			InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
			BufferedReader br = new BufferedReader(isr);
			// 简写如下
			// BufferedReader br = new BufferedReader(new InputStreamReader(
			// new
			// FileInputStream("E:/phsftp/evdokey/evdokey_201103221556.txt"),
			// "UTF-8"));
			String line = "";
			String[] arrs = null;
			while ((line = br.readLine()) != null) {
				getSql(line);
			}
			System.out.println(sb.substring(0 , sb.length() - 1).toString());
			br.close();
			isr.close();
			fis.close();
		}
	}

	/**
	 * 根据文本解析成SQL
	 * 
	 * @param inputTxt
	 * @return
	 */
	public String getSql(String inputTxt) {
		if (StringUtils.isNotEmpty(inputTxt)) {
			conventStr(inputTxt);
		}
		return null;
	}

	private String conventStr(String inputTxt) {
		String [] array = null;
		if (StringUtils.isNotBlank(inputTxt)) {
			array = inputTxt.split(",");
			checkNull(array);
		}
		return null;
	}
	
	private void checkNull(String [] array) {
		int count = 0;
		sb.append("(");
		for (String value : array) {
			if (StringUtils.isNotBlank(value)) {
				++count;
				sb.append(count != 1 ? "'" : "").append(value.trim()).append(count != 1 ? "'" : "").append(count + 1 != array.length ? "," : "");
			}
		}
		sb.append("),");
	}

}
