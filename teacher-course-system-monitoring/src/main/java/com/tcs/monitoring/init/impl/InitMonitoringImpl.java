package com.tcs.monitoring.init.impl;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcs.monitoring.init.IInitMonitoring;
import com.tcs.monitoring.task.IMonitoring;

public class InitMonitoringImpl implements IInitMonitoring {

	private final Logger logger = LoggerFactory.getLogger(InitMonitoringImpl.class);

	// 硬件
	private IMonitoring monitoringHardServers;

	// 集群网络监控
	private IMonitoring monitoringNetServers;

	// 进程列表监控
	private IMonitoring monitoringProcessServers;

	private ScheduledExecutorService executors = Executors.newScheduledThreadPool(1);

	@Override
	public void initMethod() {
		try {
			executors.scheduleAtFixedRate(monitoringHardServers, 0, 500, TimeUnit.MILLISECONDS);
			executors.scheduleAtFixedRate(monitoringNetServers, 0, 30, TimeUnit.MINUTES);
			executors.scheduleAtFixedRate(monitoringProcessServers, 0, 1, TimeUnit.MINUTES);
		} catch (Exception e) {
			logger.error("", e);
		}
	}

	public IMonitoring getMonitoringHardServers() {
		return monitoringHardServers;
	}

	public void setMonitoringHardServers(IMonitoring monitoringHardServers) {
		this.monitoringHardServers = monitoringHardServers;
	}

	public IMonitoring getMonitoringNetServers() {
		return monitoringNetServers;
	}

	public void setMonitoringNetServers(IMonitoring monitoringNetServers) {
		this.monitoringNetServers = monitoringNetServers;
	}

	public IMonitoring getMonitoringProcessServers() {
		return monitoringProcessServers;
	}

	public void setMonitoringProcessServers(IMonitoring monitoringProcessServers) {
		this.monitoringProcessServers = monitoringProcessServers;
	}
}