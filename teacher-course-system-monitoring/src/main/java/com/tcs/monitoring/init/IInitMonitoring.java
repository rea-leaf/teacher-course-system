package com.tcs.monitoring.init;

/**
 * 初始化
 * @author wangbo
 *
 */
public interface IInitMonitoring {
	
	void initMethod();

}