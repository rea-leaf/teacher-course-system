package com.tcs.monitoring.task.impl;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcs.model.monitoring.info.AbstractMonitoringInfo;
import com.tcs.model.monitoring.info.MonitoringNetInfo;
import com.tcs.monitoring.task.AbstractMonitoring;
import com.tcs.util.net.Ping;

public class MonitoringNetServersImpl extends AbstractMonitoring {

	private final Logger logger = LoggerFactory.getLogger(MonitoringNetServersImpl.class);

	private String[] ipArray = { "192.168.1.111", "192.168.1.113", "192.168.1.114" };

	private AbstractMonitoringInfo abstractMonitoringInfo = new MonitoringNetInfo();

	public void monitoring() {
		boolean isPing = false;
		for (String ip : this.ipArray) {
			try {
				isPing = Ping.ping(ip, 5);
				this.logger.info(" ip: {} , 是否ping 通 : {}", ip, Boolean.valueOf(isPing));
				if (!isPing) {
					this.abstractMonitoringInfo.setSubject("网络集群扫描引擎扫描出警告 : " + ip + " 节点网络掉线!").setContent(
							"================================================================= \n网络集群扫描引擎扫描出警告 : " + ip
									+ " 节点网络掉线,请检查服务器是否挂掉!\n" + "如果在短时间内未能收到告警.将确认服务器挂掉, \n" + "请重启服务器!");

					this.iSendService.sendObject(this.abstractMonitoringInfo);
				}
			} catch (IOException e) {
				this.logger.error("", e);
			}
		}
	}
}