package com.tcs.monitoring.task.impl;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.tcs.model.monitoring.info.AbstractMonitoringInfo;
import com.tcs.model.monitoring.info.MonitoringBean;
import com.tcs.model.monitoring.info.MonitoringDetalBean;
import com.tcs.model.monitoring.info.MonitoringProcessInfo;
import com.tcs.monitoring.task.AbstractMonitoring;
import com.tcs.util.command.CommandUtil;
import com.tcs.util.uuid.UUIDUtil;

/**
 * 监控进程
 * @author wangbo
 */
public class MonitoringProcessServersImpl extends AbstractMonitoring {

	private final Logger logger = LoggerFactory.getLogger(MonitoringProcessServersImpl.class);

	private AbstractMonitoringInfo abstractMonitoringInfo = new MonitoringProcessInfo();

	private String[] commandArray = new String[] { " jps -lmvv" };

	private String result = null;
	
	private List<MonitoringDetalBean> MonitoringBeanList = null;
	
	private MonitoringBean monitoringBean = null;
	
	private int cacheSize;		// 缓存

	@Override
	public void monitoring() {
		try {
			abstractMonitoringInfo.setMonitoringProperty(super.property());
		} catch (UnknownHostException e2) {
			logger.error("", e2);
		}
		if (null != commandArray && commandArray.length > 0) {
			for (String command : commandArray) {
				try {
					result = CommandUtil.execCommand(command);
				} catch (IOException e) {
					logger.error("", e);
					try {
						iSendService.sendObject(abstractMonitoringInfo
								.setSubject(abstractMonitoringInfo.getMonitoringProperty().getIp() + " 节点进程扫描出现异常.请留意此邮件!")
								.setContent(e.getCause().getMessage()));
					} catch (Exception e1) {
						logger.error("", e1);
					}
				}
				try {
					if (StringUtils.isNotBlank(result)) {
						String [] resultArray = result.split("\\$");
						MonitoringBeanList = Lists.newArrayListWithCapacity(resultArray.length);
						String id = UUIDUtil.getUUID();
						monitoringBean = new MonitoringBean(id, monitoringProperty.getIp(), MonitoringBeanList);
						result = result.replace("$", " \n \n");
						for (String commd : resultArray) {
							if (StringUtils.isNotBlank(commd)) {
								MonitoringBeanList.add(new MonitoringDetalBean(UUIDUtil.getUUID(), monitoringProperty.getIp(), commd, id));
							}
						}
//						iMonitoringServer.insertOne(monitoringBean);
						if (cacheSize != resultArray.length) {
							cacheSize = resultArray.length;			// 缓存处理
							iSendService.sendObject(abstractMonitoringInfo
									.setSubject(abstractMonitoringInfo.getMonitoringProperty().getIp() + " 节点进程扫描结果 : ")
									.setContent("发现服务器进程有变化,请管理员查看是否正常? \n" + result));
							logger.info("abstractMonitoringInfo : {} , result : {} \n ", abstractMonitoringInfo,
									result);
						}
					}
				} catch (Exception e) {
					logger.error("", e);
					try {
						iSendService.sendObject(abstractMonitoringInfo
								.setSubject(abstractMonitoringInfo.getMonitoringProperty().getIp() + " 节点进程扫描出现异常.请留意此邮件!")
								.setContent(e.getCause().getMessage()));
					} catch (Exception e1) {
						logger.error("", e1);
					}
				} finally {
					result = null;
					MonitoringBeanList = null;
				}
			}
		}
	}
}