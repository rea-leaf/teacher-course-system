package com.tcs.monitoring.task;

/**
 * 监控
 * @author wangbo
 */
public interface IMonitoring extends Runnable {
	
	/**
	 * 监控数据
	 */
	void monitoring();
	
}