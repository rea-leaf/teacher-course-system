package com.tcs.monitoring.servers.impl;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcs.model.email.MailSenderInfo;
import com.tcs.model.monitoring.info.AbstractMonitoringInfo;
import com.tcs.model.rule.RuleBean;
import com.tcs.monitoring.servers.ISendService;
import com.tcs.rpc.service.IEmailService;
import com.tcs.server.rule.IRuleService;
import com.tcs.util.constant.SystemConstants;

public class SendServiceImpl implements ISendService {
	
	private final Logger logger = LoggerFactory.getLogger(SendServiceImpl.class);

	private IEmailService iEmailService;
	
	private MailSenderInfo mailInfo;
	
	@Resource(name="iRuleService")
	private IRuleService iRuleService;
	
	private String [] cCs = null;

	@Override
	public boolean sendObject(AbstractMonitoringInfo abstractMonitoringInfo) {
		boolean result = true;
		setCcs();
		mailInfo = new MailSenderInfo();
		mailInfo.setSubject(abstractMonitoringInfo.getSubject());
		mailInfo.setContent(abstractMonitoringInfo.getContent());
		mailInfo.setCcs(cCs);
		if (null != mailInfo) {
			iEmailService.send(mailInfo);
			logger.debug("发送成功! 数据为 : {} " , mailInfo);
		}
		return result;
	}
	
	/**
	* @Title: setCcs  
	* @Description: 设置抄送邮箱
	* @return boolean    返回类型  
	 */
	private boolean setCcs() {
		boolean result = false;
		try {
			cCs = iRuleService.getCcs(new RuleBean(SystemConstants.ProductConstants.MONITORING, 0));
			if (null != cCs && cCs.length > 0) {
				result = true;
			}
		} catch (Exception e) {
			logger.error("" , e);
		}
		return result;
	}

	public IEmailService getiEmailService() {
		return iEmailService;
	}

	public void setiEmailService(IEmailService iEmailService) {
		this.iEmailService = iEmailService;
	}

}