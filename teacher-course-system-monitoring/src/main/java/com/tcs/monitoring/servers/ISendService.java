package com.tcs.monitoring.servers;

import com.tcs.model.monitoring.info.AbstractMonitoringInfo;

/**
 * 发送数据
 * @author wangbo
 */
public interface ISendService {
	
	/**
	 * 发送数据
	 * @param monitoringInfo
	 * @return
	 */
	boolean sendObject(AbstractMonitoringInfo abstractMonitoringInfo);

}