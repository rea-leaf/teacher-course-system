package com.tcs.monitoring.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
	
	private final Logger logger = LoggerFactory.getLogger(App.class);
	
	private Object lock = new Object();
	
	public static void main(String [] args) {
		new App().run();
	}

	private void run() {
		logger.debug(" 监控程序启动!");
		logger.info(" 监控程序启动!");
		synchronized (lock) {
			ClassPathXmlApplicationContext app = new ClassPathXmlApplicationContext("classpath:spring/app.xml");
			try {
				app.registerShutdownHook();
				logger.debug(" 监控程序启动!");
				logger.info(" 监控程序启动!");
				lock.wait();
			} catch (InterruptedException e) {
				logger.error(" 锁失败!" , e);
			}
		}
	}
}