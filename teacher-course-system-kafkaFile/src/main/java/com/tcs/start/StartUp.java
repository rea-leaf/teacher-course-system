package com.tcs.start;

import com.tcs.task.GoTask;
import com.tcs.task.impl.GoTeskImpl;

public class StartUp {

	public static void main(String[] args) {
		GoTask goTask = new GoTeskImpl();
		goTask.start();
	}
}
