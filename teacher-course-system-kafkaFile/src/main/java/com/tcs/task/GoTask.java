package com.tcs.task;

/**
* @Title: GoTask.java
* @Package com.tcs.task
* @author 神经刀
* @date 2018年3月28日
* @version V1.0
 */
public interface GoTask {

	void run();
	
	void start();
}