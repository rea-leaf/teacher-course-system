package com.tcs.task.impl;

import com.tcs.task.GoTask;
import com.tcs.task.mq.MiniKafkaCustomer;

public class GoTeskImpl implements GoTask {
	
	private MiniKafkaCustomer KafkaCustomer;
	
	public GoTeskImpl() {
		KafkaCustomer = new MiniKafkaCustomer();
	}

	@Override
	public void run() {
		new Thread(KafkaCustomer , "kafka_customer").start();;
	}
	
	@Override
	public void start() {
		this.run();
	}
	
	
}