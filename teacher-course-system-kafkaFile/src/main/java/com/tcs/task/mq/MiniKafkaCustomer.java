package com.tcs.task.mq;

import java.util.Arrays;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcs.task.mq.config.KafkaConfig;
/**
* @Title: MiniKafkaCustomer.java
* @Package com.tcs.task.mq
* @author 神经刀
* @date 2018年3月28日
* @version V1.0
 */
public class MiniKafkaCustomer implements Runnable {
	
	private final Logger logger = LoggerFactory.getLogger(MiniKafkaCustomer.class);
	
	public MiniKafkaCustomer() {};
	
	@Override
	public void run() {
		@SuppressWarnings("resource")
		KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(KafkaConfig.config.getCustomerProperties());
		consumer.subscribe(Arrays.asList("log"));
		while (true) {
			ConsumerRecords<String, String> records = consumer.poll(100L);
			for (ConsumerRecord<String, String> record : records) {
				logger.debug(" topic : {} , partition : {} , offset : {}, value : {} ", record.topic(),
						record.partition(), record.offset(), record.value());
				logger.info(record.value());
			}
		}
	}

}
