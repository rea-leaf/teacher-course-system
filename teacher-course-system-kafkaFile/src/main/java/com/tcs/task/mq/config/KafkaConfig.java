package com.tcs.task.mq.config;

import java.util.Properties;

/**
* @Title: KafkaConfig.java
* @Package com.tcs.test.kafka.config
* @author 神经刀
* @date 2018年3月28日
* @version V1.0
 */
public enum KafkaConfig {
	
	config;
	
	public Properties getCustomerProperties () {
		Properties props = new Properties();
		props.put("bootstrap.servers", "192.168.1.111:9092");
		props.put("group.id", "poll1");
		props.put("enable.auto.commit", "true");
		props.put("auto.commit.interval.ms", "1000");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		return props;
	}
}